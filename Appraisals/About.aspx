﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="Appraisals.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <h3>HR Appraisals Administration.</h3>
    <p>This application provides a means to maintain core reference data associated with the City of Liverpool College Staff Appraisals system. The application is administered by the college's HR Department.</p>
</asp:Content>
