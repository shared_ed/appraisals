﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Appraisals.Admin
{
    public partial class ObjectiveTypeAmendWebForm : System.Web.UI.Page
    {
        protected class ObjectiveType
        {
            public Int32 ObjectiveTypeID { get; set; }
            public String TypeName { get; set; }
            public Boolean IsFixed { get; set; }
            public Boolean MidYearReq { get; set; }
            public Boolean YearEndReq { get; set; }
            public Boolean IsRating { get; set; }
            public Boolean IsPerformance { get; set; }
            public Boolean IsComment { get; set; }
            public Boolean IsOpen { get; set; }
            public Boolean IsActive { get; set; }
            public Boolean IsSupportReq { get; set; }
            public Int32 ObjectiveTypeRank { get; set; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!Authorise.AuthoriseUser("ObjectiveType"))
                {
                    Server.Transfer("NotAuthorised.aspx", true);
                }
                GetObjectiveTypes();
            }
        }

        private void GetObjectiveTypes()
        {
            try
            {
                List<ObjectiveType> ObjectiveTypeList = new List<ObjectiveType>();

                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_GetObjectiveTypes", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        SQLConnection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (!reader.HasRows)
                            {
                                messages.InnerHtml = "There are currently no Objective Types set.";
                                messages.Visible = true;
                                return;
                            }
                            else
                            {
                                while (reader.Read())
                                    ObjectiveTypeList.Add(new ObjectiveType()
                                    {
                                        ObjectiveTypeID = (Int32)reader["ObjectiveTypeID"],
                                        TypeName = reader["TypeName"].ToString(),
                                        IsFixed = (Boolean)reader["IsFixed"],
                                        MidYearReq = (Boolean)reader["MidYearReq"],
                                        YearEndReq = (Boolean)reader["YearEndReq"],
                                        IsRating = (Boolean)reader["IsRating"],
                                        IsPerformance = (Boolean)reader["IsPerformance"],
                                        IsComment = (Boolean)reader["IsComment"],
                                        IsOpen = (Boolean)reader["IsOpen"],
                                        IsActive = (Boolean)reader["IsActive"],
                                        IsSupportReq = (Boolean)reader["IsSupportReq"],
                                        ObjectiveTypeRank = (Int32)reader["ObjectiveTypeRank"],
                                    });
                            }
                        }
                    }
                }
                objectiveTypes.DataSource = ObjectiveTypeList;
                objectiveTypes.DataBind();
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(GetObjectiveTypes) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(GetObjectiveTypes) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }

        protected void CommandBtn_Click(Object sender, CommandEventArgs e)
        {
            errors.InnerHtml = String.Empty;
            errors.Visible = false;
            messages.Visible = false;

            switch (e.CommandName)
            {
                case "Save":

                    ValidateObjectTypes();

                    if (errors.Visible == false)
                    {
                        messages.InnerHtml = "Your changes have been successfully saved.";
                        messages.Visible = true;
                    }
                    break;

                default:
                    break;
            }
        }

        private void ValidateObjectTypes()
        {
            try
            {
                List<ObjectiveType> validObjectiveTypes = new List<ObjectiveType>();
                ObjectiveType validObjective;
                Int32 parsedRank = 0;
                Boolean success = true;
                errors.InnerHtml = String.Empty;

                foreach (RepeaterItem item in objectiveTypes.Items)
                {
                    if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                    {
                        var tbObjectiveTypeID = (TextBox)item.FindControl("ObjectiveTypeID");
                        var tbTypeName = (TextBox)item.FindControl("TypeName");
                        var tbObjectiveTypeRank = (TextBox)item.FindControl("ObjectiveTypeRank");

                        if (tbTypeName.Text.Length == 0)
                        {
                            if (errors.InnerHtml.Length > 0)
                            {
                                errors.InnerHtml = errors.InnerHtml + "<br/>" + "Please provide a 'Type Name' for Objective ID " + tbObjectiveTypeID.Text + ".";
                            }
                            else
                            {
                                errors.InnerHtml = "Please provide a 'Type Name' for Objective ID " + tbObjectiveTypeID.Text + ".";
                            }
                            success = false;
                        }

                        if (tbObjectiveTypeRank.Text.Length == 0)
                        {
                            if (errors.InnerHtml.Length > 0)
                            {
                                errors.InnerHtml = errors.InnerHtml + "<br/>" + "Please provide a 'Rank' for Objective ID " + tbObjectiveTypeID.Text + ".";
                            }
                            else
                            {
                                errors.InnerHtml = "Please provide a 'Rank' for Objective ID " + tbObjectiveTypeID.Text + ".";
                            }
                            success = false;
                        } 
                        else
                        {
                            if (!Int32.TryParse(tbObjectiveTypeRank.Text, out parsedRank))
                            {
                                if (errors.InnerHtml.Length > 0)
                                {
                                    errors.InnerHtml = errors.InnerHtml + "<br/>" + "Please provide a valid integer 'Rank' for Objective ID " + tbObjectiveTypeID.Text + ".";
                                }
                                else
                                {
                                    errors.InnerHtml = "Please provide a valid integer 'Rank' for Objective ID " + tbObjectiveTypeID.Text + ".";
                                }
                                success = false;
                            }
                        }

                        if (success)
                        {
                            validObjective = new ObjectiveType();
                            validObjective.ObjectiveTypeID = Int32.Parse(tbObjectiveTypeID.Text);
                            validObjective.TypeName = tbTypeName.Text;
                            HtmlInputCheckBox chk = (HtmlInputCheckBox)item.FindControl("IsFixed");
                            validObjective.IsFixed = chk.Checked;
                            chk = (HtmlInputCheckBox)item.FindControl("MidYearReq");
                            validObjective.MidYearReq = chk.Checked;
                            chk = (HtmlInputCheckBox)item.FindControl("YearEndReq");
                            validObjective.YearEndReq = chk.Checked;
                            chk = (HtmlInputCheckBox)item.FindControl("IsRating");
                            validObjective.IsRating = chk.Checked;
                            chk = (HtmlInputCheckBox)item.FindControl("IsPerformance");
                            validObjective.IsPerformance = chk.Checked;
                            chk = (HtmlInputCheckBox)item.FindControl("IsComment");
                            validObjective.IsComment = chk.Checked;
                            chk = (HtmlInputCheckBox)item.FindControl("IsOpen");
                            validObjective.IsOpen = chk.Checked;
                            chk = (HtmlInputCheckBox)item.FindControl("IsActive");
                            validObjective.IsActive = chk.Checked;
                            chk = (HtmlInputCheckBox)item.FindControl("IsSupportReq");
                            validObjective.IsSupportReq = chk.Checked;
                            chk = (HtmlInputCheckBox)item.FindControl("MidYearReq");
                            validObjective.ObjectiveTypeRank = parsedRank;
                            validObjectiveTypes.Add(validObjective);
                        }
                    }
                }

                if (success)
                {
                    errors.Visible = false;
                    foreach (ObjectiveType objType in validObjectiveTypes)
                    {
                        UpdateObjectiveType(objType);
                    }
                }
                else
                {
                    errors.Visible = true;
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(ValidateObjectTypes) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(ValidateObjectTypes) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }

        private void UpdateObjectiveType (ObjectiveType objType)
        {
            try
            {
                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("[sp_UpdateObjectiveType]", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@ObjectiveTypeID", SqlDbType.Int).Value = objType.ObjectiveTypeID;
                        command.Parameters.Add("@TypeName", SqlDbType.VarChar).Value = objType.TypeName;
                        command.Parameters.Add("@IsFixed", SqlDbType.Bit).Value = objType.IsFixed;
                        command.Parameters.Add("@MidYearReq", SqlDbType.Bit).Value = objType.MidYearReq;
                        command.Parameters.Add("@YearEndReq", SqlDbType.Bit).Value = objType.YearEndReq;
                        command.Parameters.Add("@IsRating", SqlDbType.Bit).Value = objType.IsRating;
                        command.Parameters.Add("@IsPerformance", SqlDbType.Bit).Value = objType.IsPerformance;
                        command.Parameters.Add("@IsComment", SqlDbType.Bit).Value = objType.IsComment;
                        command.Parameters.Add("@IsOpen", SqlDbType.Bit).Value = objType.IsOpen;
                        command.Parameters.Add("@IsActive", SqlDbType.Bit).Value = objType.IsActive;
                        command.Parameters.Add("@IsSupportReq", SqlDbType.Bit).Value = objType.IsSupportReq;
                        command.Parameters.Add("@ObjectiveTypeRank", SqlDbType.Int).Value = objType.ObjectiveTypeRank;

                        SQLConnection.Open();

                        try
                        {
                            int updateCount = command.ExecuteNonQuery();

                            if (updateCount == 0)
                            {
                                errors.InnerHtml = "Unable to save changes for Objective ID:" + objType.ObjectiveTypeID + ". Sorry, no further details are available.";
                                errors.Visible = true;
                                messages.Visible = false;
                            }
                        }
                        catch (Exception ex)
                        {
                            errors.InnerHtml = "An error occurred while attempting to save your changes for Objective ID:" + objType.ObjectiveTypeID + ". (" + ex.Message + ")";
                            errors.Visible = true;
                            messages.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(UpdateObjectiveType) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(UpdateObjectiveType) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }

    }
}