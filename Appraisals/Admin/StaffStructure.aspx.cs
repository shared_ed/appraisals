﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Security.Principal;

namespace Appraisals.Admin
{
    public partial class StaffStructureWebForm : System.Web.UI.Page
    {
        private Int32 parsedMgrId;
        private Int32 parsedStaffId;
        private DateTime parsedStartDate;
        private DateTime parsedEndDate;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                errors.Visible = false;
                messages.Visible = false;
                staffDetailsSection.Visible = false;
            }
            else
            {
                if (!Authorise.AuthoriseUser("StaffStructure"))
                {
                    Server.Transfer("NotAuthorised.aspx", true);
                }
                startDate.Value = DateTime.Now.ToShortDateString();
            }
        }

        protected void CommandBtn_Click(Object sender, CommandEventArgs e)
        {
            

            switch (e.CommandName)
            {

                case "Verify":
                    errors.InnerHtml = String.Empty;
                    errors.Visible = false;

                    Boolean success = true;

                    if (managerID.Value.Length == 0)
                    {
                        errors.InnerHtml = "Please provide a manager 'Staff ID'.";
                        success = false;
                    }
                    else
                    {
                        if (!Int32.TryParse(managerID.Value, out parsedMgrId))
                        {
                            if (errors.InnerHtml.Length > 0)
                            {
                                errors.InnerHtml = errors.InnerHtml + "<br/>" + "'Staff ID' of manager is incorrect. Please enter a numeric value.";
                            }
                            else
                            {
                                errors.InnerHtml = "'Staff ID' of manager is incorrect. Please enter a numeric value.";
                            }
                            success = false;
                        }
                    }

                    if (StaffID.Value.Length == 0)
                    {
                        if (errors.InnerHtml.Length > 0)
                        {
                            errors.InnerHtml = errors.InnerHtml + "<br/>" + "Please provide a 'Staff ID' for the colleague being appraised.";
                        }
                        else
                        {
                            errors.InnerHtml = "Please provide a 'Staff ID' for the colleague being appraised.";
                        }
                        success = false;
                    }
                    else
                    {
                        if (!Int32.TryParse(StaffID.Value, out parsedStaffId))
                        {
                            if (errors.InnerHtml.Length > 0)
                            {
                                errors.InnerHtml = errors.InnerHtml + "<br/>" + "'Staff ID' of employee is incorrect. Please enter a numeric value.";
                            }
                            else
                            {
                                errors.InnerHtml = "'Staff ID' of employee is incorrect. Please enter a numeric value.";
                            }
                            success = false;
                        }
                    }

                    if (!DateTime.TryParse(startDate.Value, out parsedStartDate))
                    {
                        if (errors.InnerHtml.Length > 0)
                        {
                            errors.InnerHtml = errors.InnerHtml + "<br/>" + "'Start Date' is incorrect. Please enter a valid date in dd/mm/yyyy format.";
                        }
                        else
                        {
                            errors.InnerHtml = "'Start Date' is incorrect. Please enter a valid date in dd/mm/yyyy format.";
                        }
                        success = false;
                    }

                    if (endDate.Value.Length != 0) //allow blank end date
                    {
                        if (!DateTime.TryParse(endDate.Value, out parsedEndDate))
                        {
                            if (errors.InnerHtml.Length > 0)
                            {
                                errors.InnerHtml = errors.InnerHtml + "<br/>" + "'End Date' is incorrect. Please enter a valid date in dd/mm/yyyy format.";
                            }
                            else
                            {
                                errors.InnerHtml = "'End Date' is incorrect. Please enter a valid date in dd/mm/yyyy format.";
                            }
                            success = false;
                        }
                    }

                    if (!success) //failed validation
                    {
                        errors.Visible = true;
                        return;
                    }

                    //validation successful so crack on....
                    success = true;

                    managerDetails.Value = GetStaffDetails(parsedMgrId);

                    if (managerDetails.Value.Length == 0)
                    {
                        if (errors.InnerHtml.Length > 0)
                        {
                            errors.InnerHtml = errors.InnerHtml + "<br/>" + "Cannot find Staff details for 'Staff ID': " + parsedMgrId;
                        }
                        else
                        {
                            errors.InnerHtml = "Cannot find Staff details for 'Staff ID': " + parsedMgrId;
                        }
                        success = false;
                    }

                    staffDetails.Value = GetStaffDetails(parsedStaffId);
                    if (staffDetails.Value.Length == 0)
                    {
                        if (errors.InnerHtml.Length > 0)
                        {
                            errors.InnerHtml = errors.InnerHtml + "<br/>" + "Cannot find Staff details for 'Staff ID': " + parsedStaffId;
                        }
                        else
                        {
                            errors.InnerHtml = "Cannot find Staff details for 'Staff ID': " + parsedStaffId;
                        }
                        success = false;
                    }
                    
                    if (success)
                    {
                        staffDetailsSection.Visible = true;
                        verifySection.Visible = false;
                        managerID.Attributes.Add("readonly", "readonly");
                        StaffID.Attributes.Add("readonly", "readonly");
                        startDate.Attributes.Add("readonly", "readonly");
                        endDate.Attributes.Add("readonly", "readonly");
                    } else
                    {
                        errors.Visible = true;
                    }

                    break;

                case "Clear":
                    managerID.Value = String.Empty;
                    StaffID.Value = String.Empty;
                    startDate.Value = DateTime.Now.ToShortDateString();
                    endDate.Value = String.Empty;
                    managerDetails.Value = String.Empty;
                    staffDetails.Value = String.Empty;
                    staffDetailsSection.Visible = false;
                    verifySection.Visible = true;
                    managerID.Attributes.Remove("readonly");
                    StaffID.Attributes.Remove("readonly");
                    startDate.Attributes.Remove("readonly");
                    endDate.Attributes.Remove("readonly");

                    break;

                case "Add":

                    Int32.TryParse(managerID.Value, out parsedMgrId);
                    Int32.TryParse(StaffID.Value, out parsedStaffId);
                    DateTime.TryParse(startDate.Value, out parsedStartDate);
                    DateTime.TryParse(endDate.Value, out parsedEndDate);
                    AddStaffStructure(parsedMgrId, parsedStaffId, parsedStartDate, parsedEndDate);

                    managerID.Value = String.Empty;
                    StaffID.Value = String.Empty;
                    startDate.Value = DateTime.Now.ToShortDateString();
                    endDate.Value = String.Empty;
                    managerDetails.Value = String.Empty;
                    staffDetails.Value = String.Empty;
                    staffDetailsSection.Visible = false;
                    verifySection.Visible = true;
                    managerID.Attributes.Remove("readonly");
                    StaffID.Attributes.Remove("readonly");
                    startDate.Attributes.Remove("readonly");
                    endDate.Attributes.Remove("readonly");

                    break;

                default:

                    break;

            }

        }

        private String GetStaffDetails(Double staffID)
        {
            try
            {
                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_GetStaffDetails", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@StaffID", SqlDbType.Float).Value = staffID;

                        SQLConnection.Open();
                        
                        using (SqlDataReader reader = command.ExecuteReader())
                        {

                            while (reader.Read())
                              
                                    return reader["Forename"].ToString() + " " + reader["Surname"].ToString() + Environment.NewLine +
                                            reader["CurrentPost"].ToString() + Environment.NewLine +
                                            reader["OrganisationUnit"].ToString() + Environment.NewLine +
                                            reader["OrganisationDivision"].ToString();
                        }
                        return String.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(GetStaffDetails) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(GetStaffDetails) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
                return String.Empty; ;
            }
        }

        private void AddStaffStructure(Int32 mgrId, Int32 staffId, DateTime startDate, DateTime endDate)
        {
            try
            {
                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("[sp_AddStaffStructure]", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@StaffMemberID", SqlDbType.Int).Value = staffId;
                        command.Parameters.Add("@ManagerID", SqlDbType.Int).Value = mgrId;
                        command.Parameters.Add("@DateFrom", SqlDbType.DateTime).Value = startDate;

                        if (endDate.ToString() != "01/01/0001 00:00:00")
                        {
                            command.Parameters.Add("@DateTo", SqlDbType.DateTime).Value = endDate;
                        }

                        SQLConnection.Open();

                        try
                        {
                            int updateCount = command.ExecuteNonQuery();

                            if (updateCount > 0)
                            {
                                messages.InnerText = "Your 'Staff Structure' change has been saved.";
                                messages.Visible = true;
                                errors.Visible = false;
                            }
                            else
                            {
                                errors.InnerHtml = "Unable to save changes. Sorry, no further details are available.";
                                errors.Visible = true;
                                messages.Visible = false;
                            }
                        }
                        catch (Exception ex)
                        {
                            errors.InnerHtml = "An error occurred while attempting to save your changes: " + ex.Message;
                            errors.Visible = true;
                            messages.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(AddStaffStructure) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(AddStaffStructure) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }
    }
}