﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.UI.HtmlControls;


namespace Appraisals.Admin
{

    public partial class SuperUsersWebForm : System.Web.UI.Page
    {
        protected class SuperUser
        {
            public String Username { get; set; }
            public String Function { get; set; }
        }

        protected class SuperUserFunction
        {
            public String Function { get; set; }
            public Boolean IsChecked { get; set; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!Authorise.AuthoriseUser())
                {
                    Server.Transfer("NotAuthorised.aspx", true);
                }
                errors.Visible = false;
                errors.InnerHtml = String.Empty;
                messages.Visible = false;
                messages.InnerHtml = String.Empty;

                GetSuperUsers();
            }
        }

        private void GetSuperUsers()
        {
            try
            {
                superUserDropDown.Items.Clear();

                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_GetAllSuperUsers", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        SQLConnection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (!reader.HasRows)
                            {
                                messages.InnerHtml = "There are currently no 'Super Users' assigned.";
                                return;
                            }
                            else
                            {
                                superUserDropDown.Items.Add(new ListItem("Please Select...",
                                                                        "Not Selected"));
                                while (reader.Read())
                                { 
                                 
                                        superUserDropDown.Items.Add(new ListItem((string)reader["Username"].ToString(),
                                                                            (string)reader["Username"].ToString()));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(GetSuperUsers) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(GetSuperUsers) : " + ex.Message;
                }
                errors.Visible = true;
            }
        }

        protected void CommandBtn_Click(Object sender, CommandEventArgs e)
        {
            errors.InnerHtml = String.Empty;
            messages.InnerHtml = String.Empty;
            errors.Visible = false;
            messages.Visible = false;

            Boolean success = true;

            switch (e.CommandName)
            {
                case "AddInitial":

                    if (username.Value.Length == 0)
                    {
                        errors.InnerHtml = "Please provide a Username for the new 'Super User'.";
                        errors.Visible = true;
                        return;
                    }

                    foreach (ListItem li in superUserDropDown.Items)
                    {
                        if (li.Value.ToUpper() == username.Value.ToUpper())
                        {
                            errors.InnerHtml = "User already exists but you may 'Delete' or 'Amend'.";
                            errors.Visible = true;
                            return;
                        }
                    }

                    GetSuperUserFunctions(superUserFunctionsInsert);
                    foreach (RepeaterItem item in superUserFunctionsInsert.Items)
                    {
                        if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                        {
                            HtmlInputCheckBox chk = (HtmlInputCheckBox)item.FindControl("FunctionInsert");
                            HtmlGenericControl lblFunctionName = (HtmlGenericControl)item.FindControl("FunctionNameInsert");

                            if (lblFunctionName.InnerText == "Console")
                            {
                                chk.Checked = true;
                                chk.Disabled = true;
                            }
                        }
                    }
                    AddInitialRow.Visible = false;
                    functionsAddRow.Visible = true;
                    username.Attributes.Add("readonly", "readonly");

                    break;

                case "AddFinal":
                
                    foreach (RepeaterItem item in superUserFunctionsInsert.Items)
                    {
                        if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                        {
                            HtmlInputCheckBox chk = (HtmlInputCheckBox)item.FindControl("FunctionInsert");
                            HtmlGenericControl lblFunctionName = (HtmlGenericControl)item.FindControl("FunctionNameInsert");

                            if (chk.Checked)
                            {
                                if (!AddSuperUser(username.Value, lblFunctionName.InnerText))
                                {
                                    success = false;
                                }
                            }
                        }
                    }

                    if (success)
                    {
                        GetSuperUsers();

                        messages.InnerHtml = "'" + username.Value.ToUpper() + "' has been added to the list of 'Super Users'";
                        messages.Visible = true;

                        AddInitialRow.Visible = true;
                        functionsAddRow.Visible = false;
                        username.Value = String.Empty;
                        username.Attributes.Remove("readonly");
                    }

                    break;

                case "Delete":

                    if (superUserDropDown.SelectedValue == "Not Selected")
                    {
                        errors.InnerHtml = "Please select a 'Super User' to delete.";
                        errors.Visible = true;
                        return;
                    }
                        
                    if (DeleteSuperUser(superUserDropDown.SelectedValue))
                    {
                        messages.InnerHtml = "'" + superUserDropDown.SelectedValue.ToUpper() + "' has been deleted from the list of 'Super Users'";
                        messages.Visible = true;

                        GetSuperUsers();
                    }

                    break;

                case "AmendInitial":

                    if (superUserDropDown.SelectedValue == "Not Selected")
                    {
                        errors.InnerHtml = "Please select a 'Super User' to amend.";
                        errors.Visible = true;
                        return;
                    }

                    GetSuperUser(superUserDropDown.SelectedValue);
                    amendFunctionRow.Visible = true;
                    InitButtonRow.Visible = false;

                    break; 

                case "AmendFinal":

                    foreach (RepeaterItem item in superUserFunctionsAmend.Items)
                    {
                        if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                        {
                            HtmlInputCheckBox chk = (HtmlInputCheckBox)item.FindControl("FunctionAmend");
                            HtmlGenericControl lblFunctionName = (HtmlGenericControl)item.FindControl("FunctionNameAmend");

                            if (chk.Checked)
                            {
                                if (!AddSuperUser(superUserDropDown.SelectedValue, lblFunctionName.InnerText))
                                {
                                    success = false;
                                }
                            }
                            else
                            {
                                if (!DeleteSuperUser(superUserDropDown.SelectedValue, lblFunctionName.InnerText))
                                {
                                    success = false;
                                }
                            }
                        }
                    }
                    if (success)
                    {
                        messages.InnerHtml = "'Super User '" + superUserDropDown.SelectedValue + "' has been amended. ";
                        messages.Visible = true;

                        amendFunctionRow.Visible = false;
                        InitButtonRow.Visible = true;
                        superUserDropDown.SelectedValue = "Not Selected";
                    }

                    break;

                case "Reset":

                    amendFunctionRow.Visible = false;
                    InitButtonRow.Visible = true;
                    superUserDropDown.SelectedValue = "Not Selected";

                    break;

                default:
                    break;
            }
        }

        protected Boolean AddSuperUser(String username, String function)
        {
            try
            {
                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_AddSuperUser", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@Username", SqlDbType.VarChar).Value = username.ToUpper();
                        command.Parameters.Add("@Function", SqlDbType.VarChar).Value = function;

                        SQLConnection.Open();

                        try
                        {
                            int updateCount = command.ExecuteNonQuery();

                            if (updateCount == 0)
                            {
                                errors.InnerHtml = "Unable to add new 'Super User' for " + username.ToUpper() + ", (Function:'" + function + "'). Sorry, no further details are available.";
                                errors.Visible = true;
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }
                        catch (Exception ex)
                        {
                            errors.InnerHtml = "An error occurred while attempting to add new 'Super User' for " + username.ToUpper() + ", (Function:'" + function + "') : " + ex.Message;
                            errors.Visible = true;
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(AddSuperUser) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(AddSuperUser) : " + ex.Message;
                }
                errors.Visible = true;
                return false;
            }
        }

        protected Boolean DeleteSuperUser(String username, String function = null)
        {
            try
            {
                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_DeleteSuperUser", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@Username", SqlDbType.VarChar).Value = username.ToUpper();
                        if (function != null)
                        {
                            command.Parameters.Add("@Function", SqlDbType.VarChar).Value = function;
                        }
                        SQLConnection.Open();

                        try
                        {
                            int updateCount = command.ExecuteNonQuery();

                            if (updateCount == 0)
                            {
                                errors.InnerHtml = "Unable to delete 'Super User'. Sorry, no further details are available.";
                                errors.Visible = true;
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }
                        catch (Exception ex)
                        {
                            errors.InnerHtml = "An error occurred while attempting to delete 'Super User': " + ex.Message;
                            errors.Visible = true;
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(DeleteSuperUser) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(DeleteSuperUser) : " + ex.Message;
                }
                errors.Visible = true;
                return false;
            }
        }

        private void GetSuperUser(String username)
        {
            try
            {
                List<SuperUserFunction> FunctionList = new List<SuperUserFunction>();
                String functionName = String.Empty;

                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_GetSuperUser", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@Username", SqlDbType.VarChar).Value = username.ToUpper();

                        SQLConnection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (!reader.HasRows)
                            {
                                messages.InnerHtml = "The 'Super User' cannot be found in the database.";
                                messages.Visible = true;
                                return;
                            }
                            else
                            {
                                while (reader.Read())
                                    FunctionList.Add(new SuperUserFunction()
                                    {
                                        Function = reader["Function"].ToString(),
                                        IsChecked = Convert.ToBoolean(reader["ischecked"])
                                    });
                            }
                        }
                    }
                }
                superUserFunctionsAmend.DataSource = FunctionList;
                superUserFunctionsAmend.DataBind();
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(GetSuperUser) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(GetSuperUser) : " + ex.Message;
                }
                errors.Visible = true;
            }
        }

        private void    GetSuperUserFunctions(Repeater repeater)
        {
            try
            {
                List<SuperUserFunction> FunctionList = new List<SuperUserFunction>();

                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_GetSuperUserFunctions", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        SQLConnection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (!reader.HasRows)
                            {
                                messages.InnerHtml = "There are currently no 'Super User' functions set.";
                                messages.Visible = true;
                                return;
                            }
                            else
                            {
                                while (reader.Read())
                                    FunctionList.Add(new SuperUserFunction()
                                    {
                                        Function = reader["Function"].ToString(),
                                        IsChecked = false
                                    });
                            }
                        }
                    }
                }
                repeater.DataSource = FunctionList;
                repeater.DataBind();
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(GetSuperUserFunctions) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(GetSuperUserFunctions) : " + ex.Message;
                }
                errors.Visible = true;
            }
        }

    }
}