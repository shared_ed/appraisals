﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Site.Master" AutoEventWireup="true" CodeBehind="Reports.aspx.cs" Inherits="Appraisals.Admin.ReportsWebForm" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="well well-sm" style="text-align:center">
        <h1 id="objTypeTitle" runat="server">Appraisal Reports Index</h1>
    </div>

    <div id="messages" class="alert alert-success alert-dismissible" runat="server" visible="false">
    </div>
    <div id="errors" class="alert alert-danger alert-dismissible" runat="server" visible="false">
    </div>
    
    <div class="panel panel-default">
        <div class="panel-heading">Index of reports relating to HR Appraisals.</div>
        <div class="panel-body">

       
  
        <div class="row" runat="server">
            <div class="col-md-12">
                <div class="form-group">
                    Click on associated 'Link' to view a folder/report.
                </div>
            </div>
        </div>


        <div class="row">
                <div class="col-md-12">
                    <table class="table">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Link</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="reports" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td><asp:TextBox  class="form-control" ReadOnly="true" ID="title" runat="server" Text='<%# Eval("title") %>' /></td>
                                    <td><asp:TextBox class="form-control"  ReadOnly="true" ID="description" runat="server" Text='<%# Eval("description") %>'  /></td>
                                    <td class="form-control" style="margin-top:6px;"><%# Eval("link") %></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
        </div>
</asp:Content>
