﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Appraisals.Admin
{
    static class Authorise
    {
         public static Boolean AuthoriseUser(string callingApp = "")
        {
            try
            {
                Boolean accessAllowed = false;

                String username = System.Web.HttpContext.Current.User.Identity.Name;
                username = username.Substring(username.LastIndexOf('\\') + 1);

                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_GetAdminStaffPermissions", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@UserID", SqlDbType.VarChar).Value = username; 

                        SQLConnection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                accessAllowed = true;
                            }
                        }
                    }
                }

                //check if access allowed as SuperUuser of given function (i.e. calling application)
                if(!accessAllowed && callingApp.Length != 0)
                {
                    using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                    {
                        using (SqlCommand command = new SqlCommand("sp_GetSuperUserPermissions", SQLConnection))
                        {
                            command.CommandType = CommandType.StoredProcedure;

                            command.Parameters.Add("@UserID", SqlDbType.VarChar).Value = username;
                            command.Parameters.Add("@Function", SqlDbType.VarChar).Value = callingApp;

                            SQLConnection.Open();

                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    accessAllowed = true;
                                }
                            }
                        }
                    }
                }
                return accessAllowed;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }
}