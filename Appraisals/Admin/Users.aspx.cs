﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Appraisals.Admin
{
   
    public partial class UsersWebForm : System.Web.UI.Page
    {
        protected class AdminUser
        {
            public String Username { get; set; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!Authorise.AuthoriseUser("Users"))
                {
                    Server.Transfer("NotAuthorised.aspx", true);
                }
                GetAdmins();
            }
        }

        private void GetAdmins()
        {
            try
            {
                List<AdminUser> adminsList = new List<AdminUser>();

                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_GetAdministrators", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        SQLConnection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (!reader.HasRows)
                            {
                                messages.InnerHtml = "There are currently no Appraisal Administrators assigned.";
                                messages.Visible = true;
                                return;
                            }
                            else
                            {
                                while (reader.Read())
                                    adminsList.Add(new AdminUser()
                                    {
                                        Username = reader["Username"].ToString(),
                                    });
                            }
                        }
                    }
                }
                administrators.DataSource = adminsList;
                administrators.DataBind();
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(GetAdmins) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(GetAdmins) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }

        protected void CommandBtn_Click(Object sender, CommandEventArgs e)
        {
            errors.InnerHtml = String.Empty;
            errors.Visible = false;
            messages.Visible = false;

            if (username.Value.Length == 0)
            {
                errors.InnerHtml = "Please provide a 'Username'.";
                errors.Visible = true;
            }
            else
            {
                switch (e.CommandName)
                {

                    case "Add":

                        if (AddAdminUser(username.Value))
                        {
                            messages.InnerHtml = "User '" + username.Value.ToUpper() + "' has been added to the list of Administrators.";
                            messages.Visible = true;
                        }

                        break;

                    case "Delete":

                        if (DeleteAdminUser(username.Value))
                        {
                            messages.InnerHtml = "User '" + username.Value.ToUpper() + "' has been deleted from the list of Administrators. ";
                            messages.Visible = true;
                        }

                        break;

                    default:
                        break;
                }
            }
        }

        protected Boolean AddAdminUser(String username)
        {
            try
            {
                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("[sp_AddAdminUser]", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@Username", SqlDbType.VarChar).Value = username.ToUpper();
                        SQLConnection.Open();

                        try
                        {
                            int updateCount = command.ExecuteNonQuery();

                            if (updateCount == 0)
                            {
                                errors.InnerHtml = "Unable to add new Administrator. Sorry, no further details are available.";
                                errors.Visible = true;
                                messages.Visible = false;
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }
                        catch (Exception ex)
                        {
                            errors.InnerHtml = "An error occurred while attempting to add new Administrator: " + ex.Message;
                            errors.Visible = true;
                            messages.Visible = false;
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(AddAdminUser) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(AddAdminUser) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
                return false;
            }
        }

        protected Boolean DeleteAdminUser(String username)
        {
            try
            {
                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("[sp_DeleteAdminUser]", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@Username", SqlDbType.VarChar).Value = username.ToUpper();
                        SQLConnection.Open();

                        try
                        {
                            int updateCount = command.ExecuteNonQuery();

                            if (updateCount == 0)
                            {
                                errors.InnerHtml = "Unable to delete Administrator. Sorry, no further details are available.";
                                errors.Visible = true;
                                messages.Visible = false;
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }
                        catch (Exception ex)
                        {
                            errors.InnerHtml = "An error occurred while attempting to delete Administrator: " + ex.Message;
                            errors.Visible = true;
                            messages.Visible = false;
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(DeleteAdminUser) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(DeleteAdminUser) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
                return false;
            }
        }
    }
}