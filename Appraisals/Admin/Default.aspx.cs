﻿using System;
using System.Web.UI;
using System.Security.Principal;
using Appraisals.Admin;

namespace Appraisals
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Authorise.AuthoriseUser("Console"))
            {
                Server.Transfer("NotAuthorised.aspx",true);
            }
        }
    }
}