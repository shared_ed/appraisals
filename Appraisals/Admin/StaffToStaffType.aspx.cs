﻿using System;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Appraisals.Admin
{
    public partial class StaffToStaffTypeWebForm : System.Web.UI.Page
    {
        protected Int32 StaffTypeID;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                errors.Visible = false;
                errors.InnerHtml = String.Empty;
                messages.Visible = false;
                messages.InnerHtml = String.Empty;
            }
            else
            {
                if (!Authorise.AuthoriseUser("StaffToStaffType"))
                {
                    Server.Transfer("NotAuthorised.aspx", true);
                }

                HiddenIsInsert.Value = "False";
            }
        }

        protected void CommandBtn_Click(Object sender, CommandEventArgs e)
        {
            Int32 parsedStaffID;

            errors.Visible = false;
            errors.InnerHtml = String.Empty;
            messages.Visible = false;
            messages.InnerHtml = String.Empty;

            switch (e.CommandName)
            {
                case "Search":

                    if (staffID.Value.Length == 0)
                    {
                        errors.InnerHtml = "Please provide a 'Staff ID'.";
                        errors.Visible = true;
                        return;
                    }
                    else
                    {
                        if (!Int32.TryParse(staffID.Value, out parsedStaffID))
                        {
                            if (errors.InnerHtml.Length > 0)
                            {
                                errors.InnerHtml = errors.InnerHtml + "<br/>" + "'Staff ID' is invalid. Please enter a numeric value.";
                            }
                            else
                            {
                                errors.InnerHtml = "'Staff ID' is invalid. Please enter a numeric value.";
                            }
                            errors.Visible = true;
                            return;
                        }
                    }

                    if (GetStaffTypeForStaff(parsedStaffID))
                    {
                        if (staffTypeDropDown.Items.Count == 0)
                        {
                            PopulateStaffTypeDropdown();
                        }

                        if (HiddenIsInsert.Value == "True")
                        {
                            staffTypeDropDown.SelectedIndex = 0;
                        }
                        else
                        {
                            staffTypeDropDown.SelectedIndex = Int32.Parse(hiddenStaffTypeID.Value);
                        }

                        btnSearch.Visible = false;
                        staffID.Attributes.Add("readonly", "readonly");
                    }

                    break;

                case "Save":
                    Boolean success = true;

                    if (success)
                    {
                        if (staffTypeDropDown.SelectedIndex == 0)
                        {
                            if (errors.InnerHtml.Length > 0)
                            {
                                errors.InnerHtml = errors.InnerHtml + "<br/>" + "Please select a 'Staff Type'.";
                            }
                            else
                            {
                                errors.InnerHtml = "Please select a 'Staff Type'.";
                            }
                            success = false;
                        }
                    }

                    if (success)
                    {
                        if (HiddenIsInsert.Value == "True")
                        {
                            AddStaffType(Int32.Parse(staffID.Value), staffTypeDropDown.SelectedIndex);
                        }
                        else
                        {
                            UpdateStaffType(Int32.Parse(staffID.Value), staffTypeDropDown.SelectedIndex);
                        }
                    }
                    else
                    {
                        errors.Visible = true;
                    }
                    break;

                case "Reset":

                    staffTypeRow.Visible = false;
                    btnSearch.Visible = true;
                    staffID.Attributes.Remove("readonly");
                    staffTypeDropDown.SelectedIndex = 0;
                    staffID.Value = String.Empty;

                    break;

                default:
                    break;
            }
        }

        private Boolean GetStaffTypeForStaff(Int32 StaffID)
        {
            try
            {
                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_GetStaffTypeForPerson", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@StaffID", SqlDbType.Int).Value = staffID.Value;

                        SQLConnection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                                while (reader.Read())
                                {
                                    hiddenStaffTypeID.Value = reader["StaffTypeID"].ToString();
                                    adviceText.InnerHtml = "This Staff member is already in the system; please amend the 'Staff Type', as required, and save your change.";
                                    HiddenIsInsert.Value = "False";
                                }
                            else
                            {
                                adviceText.InnerHtml = "This Staff member has no associated 'Staff Type'; Please select a 'Staff Type' and save your change.";
                                HiddenIsInsert.Value = "True";
                            }
                        }
                        staffTypeRow.Visible = true;
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(GetStaffTypeForStaff) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(GetStaffTypeForStaff) : " + ex.Message;
                }
                errors.Visible = true;
                return false;
            }
        }

        private void PopulateStaffTypeDropdown()
        {
            try
            {
                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_getStaffTypes", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        SQLConnection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            staffTypeDropDown.Items.Add(new ListItem("Please select...", "Not Selected"));

                            while (reader.Read())
                            {
                                staffTypeDropDown.Items.Add(new ListItem((string)reader["StaffTypeDescription"].ToString(),
                                                                            (string)reader["StaffTypeID"].ToString()));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(PopulateStaffTypeDropdown) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(PopulateStaffTypeDropdown) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }

        private void AddStaffType(Int32 staffId, Int32 staffTypeId)
        {
            try
            {
                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_AddStaffToStaffType", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@StaffID", SqlDbType.Int).Value = staffId;
                        command.Parameters.Add("@StaffTypeID", SqlDbType.Int).Value = staffTypeId;

                        SQLConnection.Open();

                        try
                        {
                            int updateCount = command.ExecuteNonQuery();

                            if (updateCount == 0)
                            {
                                errors.InnerHtml = "Unable to save change. Sorry, no further details are available.";
                                errors.Visible = true;
                            }
                            else
                            {
                                messages.InnerText = "Your change has been saved.";
                                messages.Visible = true;
                            }
                        }
                        catch (Exception ex)
                        {
                            errors.InnerHtml = "An error occurred while attempting to save your changes: " + ex.Message;
                            errors.Visible = true;
                            messages.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(AddStaffType) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(AddStaffType) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }

        private void UpdateStaffType(Int32 staffId, Int32 staffTypeId)
        {
            try
            {
                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_UpdateStaffToStaffType", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@StaffID", SqlDbType.Int).Value = staffId;
                        command.Parameters.Add("@StaffTypeID", SqlDbType.Int).Value = staffTypeId;

                        SQLConnection.Open();

                        try
                        {
                            int updateCount = command.ExecuteNonQuery();

                            if (updateCount == 0)
                            {
                                errors.InnerHtml = "Unable to save change. Sorry, no further details are available.";
                                errors.Visible = true;
                            }
                            else
                            {
                                messages.InnerText = "Your change has been saved.";
                                messages.Visible = true;
                            }
                        }
                        catch (Exception ex)
                        {
                            errors.InnerHtml = "An error occurred while attempting to save your changes: " + ex.Message;
                            errors.Visible = true;
                            messages.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(UpdateStaffType) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(UpdateStaffType) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }
    }
}