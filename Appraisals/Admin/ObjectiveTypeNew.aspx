﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Site.Master" AutoEventWireup="true" CodeBehind="ObjectiveTypeNew.aspx.cs" Inherits="Appraisals.Admin.ObjectiveTypeNewWebForm" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="well well-sm" style="text-align:center">
        <h1 id="objTypeTitle" runat="server">Objective Type Maintenance - Add New</h1>
    </div>

    <div style="padding-bottom:15px;"><a class="btn btn-default" href="ObjectiveTypeAmend">Change Existing &raquo;</a></div>

    <div id="messages" class="alert alert-success alert-dismissible" runat="server" visible="false">
    </div>
    <div id="errors" class="alert alert-danger alert-dismissible" runat="server" visible="false">
    </div>
    
    <div class="panel panel-default">
        <div class="panel-heading">Objective Type attributes.</div>
        <div class="panel-body">

           <div class="row" >
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="username">Objective Type Name</label>
                        <input type="text"  class="form-control" id="TypeName" runat="server">
                    </div>
                </div>
            </div>

            <div class="row" >
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="checkbox-inline"><input type="checkbox"   id="IsFixed" runat="server"><b>Is Fixed?</b></label>
                    </div>
                </div>
            </div>

            <div class="row" >
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="checkbox-inline"><input type="checkbox"   id="MidYearReq" runat="server"><b>Required Mid-Year?</b></label>
                    </div>
                </div>
            </div>

            <div class="row" >
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="checkbox-inline"><input type="checkbox"   id="YearEndReq" runat="server"><b>Required Year-End?</b></label>
                    </div>
                </div>
            </div>

            <div class="row" >
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="checkbox-inline"><input type="checkbox"   id="IsRating" runat="server"><b>Is Rating?</b></label>
                    </div>
                </div>
            </div>

            <div class="row" >
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="checkbox-inline"><input type="checkbox"   id="IsPerformance" runat="server"><b>Is Performance?</b></label>
                    </div>
                </div>
            </div>

            <div class="row" >
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="checkbox-inline"><input type="checkbox"   id="IsComment" runat="server"><b>Is Comment?</b></label>
                    </div>
                </div>
            </div>

            <div class="row" >
                <div class="col-md-12">
                    <div class="form-group">
                         <label class="checkbox-inline"><input type="checkbox"   id="IsOpen" runat="server"><b>Is SMART?</b></label>
                    </div>
                </div>
            </div>

            <div class="row" >
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="checkbox-inline"><input type="checkbox"  id="IsActive" runat="server"><b>Is Active?</b></label>
                    </div>
                </div>
            </div>

            <div class="row" >
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="checkbox-inline"><input type="checkbox"   id="IsSupportReq" runat="server"><b>Is Support Required?</b></label>
                    </div>
                </div>
            </div>
  
            <div class="row" >
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="ObjectiveTypeRank">Objective Type Rank <i>(format: integer)</i></label>
                        <input type="text"  class="form-control" id="ObjectiveTypeRank" runat="server">
                    </div>
                </div>
            </div>

            <div class="row" >
                <div class="col-md-1">
                    <div class="form-group">
                        <asp:Button ID="btnAdd" runat="server" Text="Add" class="btn btn-default" 
                            CommandName="Add"
                            CommandArgument="None"
                            OnCommand="CommandBtn_Click"/>
                    </div>
                </div>
                 <div class="col-md-3">
                    <div class="form-group">
                    <button type="reset"  class="btn btn-default">Reset</button>
                </div>
            </div>

        </div>
    </div>
</asp:Content>
