﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Linq;
using System.Web.UI.HtmlControls;

namespace Appraisals.Admin
{
    public partial class StaffToObjectiveWebForm : System.Web.UI.Page
    {
        Boolean ChangesMade;

        protected class StaffTypeToObjectiveType
        {
            public Int32 StaffTypeID { get; set; }
            public String StaffTypeDescription { get; set; }
            public Int32 ObjectiveTypeID { get; set; }
            public String TypeName { get; set; }
        }

        protected class ObjectiveType
        {
            public Int32 ObjectiveTypeID { get; set; }
            public String TypeName { get; set; }
            public Boolean ObjectiveTypeSelected { get; set; }
            public Boolean OriginallySelected { get; set; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!Authorise.AuthoriseUser("StaffTypeToObjectiveType"))
                {
                    Server.Transfer("NotAuthorised.aspx", true);
                }

                errors.Visible = false;
                errors.InnerHtml = String.Empty;
                messages.Visible = false;
                messages.InnerHtml = String.Empty;

                PopulateStaffTypeDropdown();

                GetObjectiveTypes();
            }
        }

        private void PopulateStaffTypeDropdown()
        {
            try
            {
                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_getStaffTypes", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        SQLConnection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            staffTypeDropDown.Items.Add(new ListItem("Please select...", "Not Selected"));

                            while (reader.Read())
                            {
                                staffTypeDropDown.Items.Add(new ListItem((string)reader["StaffTypeDescription"].ToString(),
                                                                            (string)reader["StaffTypeID"].ToString()));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(PopulateStaffTypeDropdown) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(PopulateStaffTypeDropdown) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }

        private void GetObjectiveTypes()
        {
            try
            {
                List<ObjectiveType> ObjectiveTypeList = new List<ObjectiveType>();

                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_GetObjectiveTypes", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        SQLConnection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (!reader.HasRows)
                            {
                                messages.InnerHtml = "There are currently no Objective Types set.";
                                messages.Visible = true;
                                return;
                            }
                            else
                            {
                                while (reader.Read())
                                    ObjectiveTypeList.Add(new ObjectiveType()
                                    {
                                        ObjectiveTypeID = (Int32)reader["ObjectiveTypeID"],
                                        TypeName = reader["TypeName"].ToString(),
                                        ObjectiveTypeSelected = false,
                                        OriginallySelected = false,
                                    });
                            }
                        }
                    }
                }
                objectiveTypes.DataSource = ObjectiveTypeList;
                objectiveTypes.DataBind();
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(GetObjectiveTypes) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(GetObjectiveTypes) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }

        private void getObjectiveTypesForStaffType()
        {
            try
            {
                List<ObjectiveType> ObjectiveTypeList = new List<ObjectiveType>();

                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_GetObjectivesForStaffType", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@StaffTypeID", SqlDbType.Int).Value = staffTypeDropDown.SelectedValue;

                        SQLConnection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                ObjectiveTypeList.Add(new ObjectiveType()
                                {
                                    ObjectiveTypeID = (Int32)reader["ObjectiveTypeID"],
                                    TypeName = reader["TypeName"].ToString(),
                                    ObjectiveTypeSelected = true,
                                    OriginallySelected = true
                                });
                            }
                        }
                    }
                }

                TickObjectiveTypeCheckboxes(ObjectiveTypeList);
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(getObjectiveTypesForStaffType) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(getObjectiveTypesForStaffType) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }

        private void TickObjectiveTypeCheckboxes(List<ObjectiveType> ObjectiveTypeList)
        {
            try
            {
                List<ObjectiveType> validObjectiveTypes = new List<ObjectiveType>();

                foreach (RepeaterItem item in objectiveTypes.Items)
                {
                    if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                    {
                        var tbObjectiveTypeID = (TextBox)item.FindControl("ObjectiveTypeID");
                        var chkObjectiveTypeSelected = (HtmlInputCheckBox)item.FindControl("ObjectiveTypeSelected");
                        var chkOriginallySelected = (HtmlInputCheckBox)item.FindControl("OriginallySelected");

                        var checkedObjectives = ObjectiveTypeList.Where(s => s.ObjectiveTypeID == Convert.ToInt32(tbObjectiveTypeID.Text)).ToList();
                        if (checkedObjectives.Count == 0)
                        {
                            chkObjectiveTypeSelected.Checked = false;
                            chkOriginallySelected.Checked = false;
                        }
                        else
                        {
                            chkObjectiveTypeSelected.Checked = true;
                            chkOriginallySelected.Checked = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(TickObjectiveTypeCheckboxes) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(TickObjectiveTypeCheckboxes) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }

        private void UpdateStaffTypeToObjectiveType()
        {
            try
            {
                ChangesMade = false;

                foreach (RepeaterItem item in objectiveTypes.Items)
                {
                    if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                    {
                        var tbObjectiveTypeID = (TextBox)item.FindControl("ObjectiveTypeID");
                        var chkObjectiveTypeSelected = (HtmlInputCheckBox)item.FindControl("ObjectiveTypeSelected");
                        var chkOriginallySelected = (HtmlInputCheckBox)item.FindControl("OriginallySelected");

                        if (chkObjectiveTypeSelected.Checked != chkOriginallySelected.Checked)
                        {
                            using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                            {
                                String storedProcName = String.Empty;
                                if (chkObjectiveTypeSelected.Checked)
                                {
                                    storedProcName = "sp_InsertStaffTypeToObjectiveType";
                                }
                                else
                                {
                                    storedProcName = "sp_DeleteStaffTypeToObjectiveType";
                                }

                                using (SqlCommand command = new SqlCommand(storedProcName, SQLConnection))
                                {
                                    command.CommandType = CommandType.StoredProcedure;

                                    command.Parameters.Add("@ObjectiveTypeID", SqlDbType.Int).Value = Convert.ToInt32(tbObjectiveTypeID.Text);
                                    command.Parameters.Add("@StaffTypeID", SqlDbType.Int).Value = staffTypeDropDown.SelectedIndex;

                                    SQLConnection.Open();

                                    try
                                    {
                                        int updateCount = command.ExecuteNonQuery();

                                        if (updateCount == 0)
                                        {
                                            errors.InnerHtml = "Unable to save change to Objective Type " + tbObjectiveTypeID.Text + ".Sorry, no further details are available.";
                                            errors.Visible = true;
                                        }
                                        else
                                        {
                                            ChangesMade = true;
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        errors.InnerHtml = "An error occurred while attempting to save your changes to Objective Type " + tbObjectiveTypeID.Text + ": " + ex.Message;
                                        errors.Visible = true;
                                        messages.Visible = false;
                                    }
                                }
                            }
                        }
                   }
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(UpdateStaffTypeToObjectiveType) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(UpdateStaffTypeToObjectiveType) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }

        protected void CommandBtn_Click(Object sender, CommandEventArgs e)
        {
            errors.Visible = false;
            errors.InnerHtml = String.Empty;
            messages.Visible = false;
            messages.InnerHtml = String.Empty;

            switch (e.CommandName)
            {
                case "Save":
                    UpdateStaffTypeToObjectiveType();

                    if (ChangesMade && errors.Visible == false)
                    {
                        messages.InnerHtml = "Your changes have been saved.";
                        messages.Visible = true;

                        //Refresh settings after 'Save'
                        getObjectiveTypesForStaffType();
                    }

                    break;

                default:
                    break;
            }
        }

        protected void staffTypeDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (staffTypeDropDown.SelectedValue == "Not Selected")
            {
                objectiveTypesRow.Visible = false;
                buttonRow.Visible = false;
            }
            else
            {
                getObjectiveTypesForStaffType();

                objectiveTypesRow.Visible = true;
                buttonRow.Visible = true;
            }
        }
    }
}