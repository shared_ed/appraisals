﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.UI.HtmlControls;

namespace Appraisals.Admin
{
    public partial class FixedObjectivesWebForm : System.Web.UI.Page
    {
        protected class FixedObj
        {
            public Int32 FixedObjectiveID { get; set; }
            public Int32 StaffTypeID { get; set; }
            public Int32 ObjectiveTypeID { get; set; }
            public String StaffTypeDescription { get; set; }
            public String TypeName { get; set; }
            public String ObjectiveTitle { get; set; }
            public String ObjectiveText  { get; set; }
            public Int32 ObjectiveRank  { get; set; }
            public String OriginalTitle { get; set; }
            public String OriginalText { get; set; }
            public Int32 OriginalRank { get; set; }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!Authorise.AuthoriseUser("FixedObjective"))
                {
                    Server.Transfer("NotAuthorised.aspx", true);
                }
            }
            errors.Visible = false;
            errors.InnerHtml = String.Empty;
            messages.Visible = false;
            messages.InnerHtml = String.Empty;
        }

        protected void academicYearDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (academicYearDropDown.SelectedValue == "Not Selected")
            {
                amendSection.Visible = false;
                insertSection.Visible = false;

                errors.InnerHtml = "Please select an Academic Year";
                errors.Visible = true;
                return;
            }
            GetStaffTypes();

            GetObjectiveTypes();

            GetFixedObjectives();

            insertSection.Visible = true;
        }

        protected void CommandBtn_Click(Object sender, CommandEventArgs e)
        {
            errors.InnerHtml = String.Empty;
            messages.InnerHtml = String.Empty;
            errors.Visible = false;
            messages.Visible = false;

            Boolean success = true;
            Int32 parsedRank;


            if (academicYearDropDown.SelectedValue == "Not Selected")
            {
                errors.InnerHtml = "Please select an Academic Year";
                errors.Visible = true;
                return;
            }

            switch (e.CommandName)
            {
                case "Add":

                    if (StaffTypeDropDown.SelectedValue == "Not Selected")
                    {
                        errors.InnerHtml = "Please select a 'Staff Type'";
                        success = false;
                    }

                    if (ObjectiveTypeDropDown.SelectedValue == "Not Selected")
                    {
                        if (errors.InnerHtml.Length > 0)
                        {
                            String newLine = errors.InnerHtml + "<br/>" + "Please select an 'Objective Type'";
                            errors.InnerHtml = newLine;
                        }
                        else
                        {
                            errors.InnerHtml = "Please select an 'Objective Type'";
                        }
                        success = false;
                    }

                    if (ObjTitle.Value.Length == 0)
                    {
                        if (errors.InnerHtml.Length > 0)
                        {
                            String newLine = errors.InnerHtml + "<br/>" + "Please provide an 'Objective Title'";
                            errors.InnerHtml = newLine;
                        }
                        else
                        {
                            errors.InnerHtml = "Please provide an 'Objective Title'";
                        }
                        success = false;
                    }

                    if (ObjDescription.Value.Length == 0)
                    {
                        if (errors.InnerHtml.Length > 0)
                        {
                            String newLine = errors.InnerHtml + "<br/>" + "Please provide an 'Objective Description'";
                            errors.InnerHtml = newLine;
                        }
                        else
                        {
                            errors.InnerHtml = "Please provide an 'Objective Description'";
                        }
                        success = false;
                    }

                    if (ObjRank.Value.Length == 0)
                    {
                        if (errors.InnerHtml.Length > 0)
                        {
                            String newLine = errors.InnerHtml + "<br/>" + "Please provide an 'Objective Ranking'";
                            errors.InnerHtml = newLine;
                        }
                        else
                        {
                            errors.InnerHtml = "Please provide an 'Objective Ranking'";
                        }
                        success = false;
                    }
                    else
                    {
                        if (!Int32.TryParse(ObjRank.Value, out parsedRank))
                        {
                            if (errors.InnerHtml.Length > 0)
                            {
                                String newLine = errors.InnerHtml + "<br/>" + "'Ranking' must be a non-integer value.";
                                errors.InnerHtml = newLine;
                            }
                            else
                            {
                                errors.InnerHtml = "'Ranking' must be a non-integer value.";
                            }
                            success = false;
                        }
                    }

                    if (success)
                    {
                        if (AddFixedObjective(Convert.ToInt32(ObjectiveTypeDropDown.SelectedValue),
                                         Convert.ToInt32(StaffTypeDropDown.SelectedValue),
                                         ObjTitle.Value,
                                         ObjDescription.Value,
                                         Convert.ToInt32(ObjRank.Value)))
                        {
                            //Refresh display
                            ObjectiveTypeDropDown.SelectedIndex = 0;
                            StaffTypeDropDown.SelectedIndex = 0;
                            ObjTitle.Value = String.Empty;
                            ObjDescription.Value = String.Empty;
                            ObjRank.Value = String.Empty;

                            GetFixedObjectives(); 

                            messages.InnerHtml = "The new 'Fixed Objective' has been added";
                            messages.Visible = true;
                        }
                        else
                        {
                            errors.Visible = true;
                        }
                    }
                    else
                    {
                        errors.Visible = true;
                    }
                    break;

                case "Save":

                    foreach (RepeaterItem item in fixedObjectives.Items)
                    {
                        if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                        {
                            TextBox titleBefore = (TextBox)item.FindControl("OriginalTitle");
                            TextBox titleAfter = (TextBox)item.FindControl("ObjectiveTitle");
                            TextBox textBefore = (TextBox)item.FindControl("OriginalText");
                            TextBox textAfter = (TextBox)item.FindControl("ObjectiveText");
                            TextBox rankBefore = (TextBox)item.FindControl("OriginalRank");
                            TextBox rankAfter = (TextBox)item.FindControl("ObjectiveRank");

                            if (!Int32.TryParse(rankAfter.Text, out parsedRank))
                            {
                                if (errors.InnerHtml.Length > 0)
                                {
                                    String newLine = errors.InnerHtml + "<br/>" + "You have entered a non-integer 'Rank' value.";
                                    errors.InnerHtml = newLine;
                                }
                                else
                                {
                                    errors.InnerHtml = "A non-integer 'Rank' of '" + rankAfter.Text + "' meant that changes to the Objective could not be saved.";
                                }
                            }
                            else
                            {
                                if (titleBefore.Text == titleAfter.Text && textBefore.Text == textAfter.Text && rankBefore.Text == rankAfter.Text)
                                {
                                    //the luxury of doing nothing; Ahhhhh!
                                }
                                else
                                {
                                    TextBox fixedObjectiveId = (TextBox)item.FindControl("FixedObjectiveID");
                                    if (!AmendFixedObjective(Convert.ToInt32(fixedObjectiveId.Text),
                                                            titleAfter.Text,
                                                            textAfter.Text,
                                                            Convert.ToInt32(rankAfter.Text)))
                                    {
                                        success = false;
                                    }
                                }
                            }
                        }
                    }

                    if (success)
                    {
                        if (errors.InnerHtml.Length == 0)
                        {
                            GetFixedObjectives();
                            messages.InnerHtml = "All changes have been saved to the database.";
                        }
                        else
                        {
                            messages.InnerHtml = "Only valid changes have been saved to the database.";
                            errors.Visible = true;
                        }
                        messages.Visible = true;
                    }
                    else
                    {
                        errors.Visible = true;
                    }

                    break;

                default:
                    break;
            }
        }

        private void GetObjectiveTypes()
        {
            try
            {
                ObjectiveTypeDropDown.Items.Clear();

                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_GetObjectiveTypes", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        SQLConnection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (!reader.HasRows)
                            {
                                messages.InnerHtml = "No Objective Types were found.";
                                return;
                            }
                            else
                            {
                                ObjectiveTypeDropDown.Items.Add(new ListItem("Please Select...",
                                                                        "Not Selected"));
                                while (reader.Read())
                                {

                                    ObjectiveTypeDropDown.Items.Add(new ListItem((string)reader["TypeName"].ToString(),
                                                                        (string)reader["ObjectiveTypeID"].ToString()));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(GetObjectiveTypes) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(GetObjectiveTypes) : " + ex.Message;
                }
                errors.Visible = true;
            }
        }

        private void GetStaffTypes()
        {
            try
            {
                StaffTypeDropDown.Items.Clear();

                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_getStaffTypes", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        SQLConnection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (!reader.HasRows)
                            {
                                messages.InnerHtml = "No Staff Types were found. Please contact MIS.";
                                return;
                            }
                            else
                            {
                                StaffTypeDropDown.Items.Add(new ListItem("Please Select...",
                                                                        "Not Selected"));
                                while (reader.Read())
                                {

                                    StaffTypeDropDown.Items.Add(new ListItem((string)reader["StaffTypeDescription"].ToString(),
                                                                        (string)reader["StaffTypeID"].ToString()));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(GetStaffTypes) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(GetStaffTypes) : " + ex.Message;
                }
                errors.Visible = true;
            }
        }

        private void GetFixedObjectives()
        {
            try
            {
                List<FixedObj> FixedObjectiveList = new List<FixedObj>();

                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_GetFixedObjectives", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@AcademicYearID", SqlDbType.VarChar).Value = academicYearDropDown.SelectedValue;

                        SQLConnection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (!reader.HasRows)
                            {
                                //no point showing 'amend' section if nothing to show...
                                amendSection.Visible = false;

                                messages.InnerHtml = "No Fixed Obectives found for Academic Year.";
                                messages.Visible = true;
                                return;
                            }
                            else
                            {
                                amendSection.Visible = true;

                                while (reader.Read())
                                    FixedObjectiveList.Add(new FixedObj()
                                    {
                                        FixedObjectiveID = (Int32)reader["FixedObjectiveID"],
                                        ObjectiveTypeID = (Int32)reader["ObjectiveTypeID"],
                                        StaffTypeID = (Int32)reader["StaffTypeID"],
                                        TypeName = reader["TypeName"].ToString(),
                                        StaffTypeDescription = reader["StaffTypeDescription"].ToString(),
                                        ObjectiveTitle = reader["ObjectiveTitle"].ToString(),
                                        ObjectiveText = reader["ObjectiveText"].ToString(),
                                        ObjectiveRank = (Int32)reader["ObjectiveRank"],
                                        OriginalTitle = reader["ObjectiveTitle"].ToString(),
                                        OriginalText = reader["ObjectiveText"].ToString(),
                                        OriginalRank = (Int32)reader["ObjectiveRank"]
                                    });
                            }
                        }
                    }
                }
                fixedObjectives.DataSource = FixedObjectiveList;
                fixedObjectives.DataBind();

                ReformatFixedObjectives();
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(GetFixedObjectives) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(GetFixedObjectives) : " + ex.Message;
                }
                errors.Visible = true;
            }
        }

        private void ReformatFixedObjectives()
        {
            try
            {
                System.Drawing.Color background1 = System.Drawing.Color.AliceBlue;
                System.Drawing.Color background2 = System.Drawing.Color.Gainsboro;
                System.Drawing.Color currentBackground = System.Drawing.Color.Empty;
                String currentStaffDesc = String.Empty;
                TextBox staffDesc;
                TextBox objType;

                foreach (RepeaterItem item in fixedObjectives.Items)
                {
                    if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                    {
                        staffDesc = (TextBox)item.FindControl("StaffTypeDescription");
                        objType = (TextBox)item.FindControl("TypeName");

                        if (staffDesc.Text != currentStaffDesc)
                        {
                            currentStaffDesc = staffDesc.Text;

                            if (currentBackground == background1)
                            {
                                currentBackground = background2;
                            }
                            else
                            {
                                currentBackground = background1;
                            }
                        }
                        staffDesc.BackColor = currentBackground;
                        objType.BackColor = currentBackground;
                    }
                }

            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(ReformatFixedObjectives) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(ReformatFixedObjectives) : " + ex.Message;
                }
                errors.Visible = true;
            }
        }

        private Boolean AmendFixedObjective(Int32 foId,
                                        String foTitle,
                                        String foText,
                                        Int32 foRank)
        {
            try
            {
                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_UpdateFixedObjective", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@FixedObjectiveID", SqlDbType.Int).Value = foId;
                        command.Parameters.Add("@ObjectiveTitle", SqlDbType.VarChar).Value = foTitle;
                        command.Parameters.Add("@ObjectiveText", SqlDbType.VarChar).Value = foText;
                        command.Parameters.Add("@ObjectiveRank", SqlDbType.VarChar).Value = foRank;

                        SQLConnection.Open();

                        try
                        {
                            int updateCount = command.ExecuteNonQuery();

                            if (updateCount == 0)
                            {
                                errors.InnerHtml = "Unable to save changes for Fixed Objective ID: " + foId + ". Sorry, no further details are available.";
                                errors.Visible = true;
                                return false;
                            }
                            return true;
                        }
                        catch (Exception ex)
                        {
                            errors.InnerHtml = "An error occurred while attempting to save your changes for Fixed Objective ID: " + foId + ". (" + ex.Message + ")";
                            errors.Visible = true;
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(AmendFixedObjective) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(AmendFixedObjective) : " + ex.Message;
                }
                errors.Visible = true;
                return false;
            }
        }

        private Boolean AddFixedObjective(Int32 foObjectiveTypeId,
                                          Int32 foStaffTypeId,
                                          String foTitle,
                                          String foText,
                                          Int32 foRank)
        {
            try
            {
                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_AddFixedObjective", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@ObjectiveTypeID", SqlDbType.Int).Value = foObjectiveTypeId;
                        command.Parameters.Add("@StaffTypeID", SqlDbType.Int).Value = foStaffTypeId;
                        command.Parameters.Add("@AcademicYearID", SqlDbType.VarChar).Value = academicYearDropDown.SelectedValue;
                        command.Parameters.Add("@ObjectiveTitle", SqlDbType.VarChar).Value = foTitle;
                        command.Parameters.Add("@ObjectiveText", SqlDbType.VarChar).Value = foText;
                        command.Parameters.Add("@ObjectiveRank", SqlDbType.VarChar).Value = foRank;

                        SQLConnection.Open();

                        try
                        {
                            int updateCount = command.ExecuteNonQuery();

                            if (updateCount == 0)
                            {
                                errors.InnerHtml = "Unable to add new 'Fixed Objective'. Sorry, no further details are available.";
                                errors.Visible = true;
                                return false;
                            }
                            return true;
                        }
                        catch (Exception ex)
                        {
                            errors.InnerHtml = "An error occurred while attempting to save your new 'Fixed Objective': "  + ex.Message;
                            errors.Visible = true;
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(AddFixedObjective) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(AddFixedObjective) : " + ex.Message;
                }
                errors.Visible = true;
                return false;
            }
        }


    }
}