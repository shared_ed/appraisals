﻿<%@ Page Title="" Language="C#" MasterPageFile="Site.Master" AutoEventWireup="true" CodeBehind="ReviewDeadline.aspx.cs" Inherits="Appraisals.Admin.ReviewDeadlineWebForm" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">



    <div class="well well-sm" style="text-align:center">
        <h1 id="objTypeTitle" runat="server">Review Deadline Maintenance</h1>
    </div>

    <div id="messages" class="alert alert-success alert-dismissible" runat="server" visible="false">
    </div>
    <div id="errors" class="alert alert-danger alert-dismissible" runat="server" visible="false">
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Review Deadline Details</div>
        <div class="panel-body">
           
            <div class="row" runat="server">
                <div class="col-md-3">
                        <div class="form-group">
                        <label for="academicYearList">Please select an Academic Year</label>
                        <asp:DropDownList ID="academicYearList" runat="server" CssClass="form-control" AutoPostBack="True"  OnSelectedIndexChanged="Year_Change">
                            <asp:ListItem Selected="True" Value="Not Selected">Please select...</asp:ListItem>
                            <asp:ListItem  Value="17/18">17/18</asp:ListItem>
                            <asp:ListItem Value="18/19">18/19</asp:ListItem>
                            <asp:ListItem Value="19/20">19/20</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
           
            <div class="row" ID="reviewDeadlineRow"  runat="server" visible="false">
                <div class="row" runat="server">
                    <div class="col-md-12">
                      <table class="table">
                        <caption style="font-size:larger;font-weight:700;">Review Deadlines</caption>
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Academic Year</th>
                                <th>Review Period</th>
                                <th>Date From (dd/mm/yyyy)</th>
                                <th>Date To (dd/mm/yyyy)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater ID="reviewDeadline" runat="server">
                                <ItemTemplate>
                                    <tr>
                                       <td><asp:TextBox ReadOnly="true"  BorderStyle="None" ID="DeadlineID" runat="server" Text='<%# Eval("DeadlineID") %>' /></td>
                                        <td><asp:TextBox ReadOnly="true" BorderStyle="None"  ID="AcademicYearID" runat="server" Text='<%# Eval("AcademicYearID") %>' /></td>
                                        <td><asp:TextBox BorderStyle="None" ReadOnly="true" ID="ReviewPeriodID" runat="server" Text='<%# Eval("ReviewPeriodID") %>' /></td>
                                        <td><asp:TextBox BorderStyle="None" ID="DateFrom" runat="server" Text='<%# Eval("DateFrom") %>' /></td>
                                        <td><asp:TextBox BorderStyle="None" ID="DateTo" runat="server" Text='<%# Eval("DateTo") %>' /></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </div>
            </div>
            </div> 

            <div class="row" id="saveRow" runat="server" visible="false">
                 <div class="col-md-1">
                    <div class="form-group">
                        <asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-default" 
                            CommandName="Save"
                            CommandArgument="None"
                            OnCommand="CommandBtn_Click"/>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <button type="reset"  class="btn btn-default">Reset</button>
                    </div>
                </div>
            </div>

            <div class="row" id="addRow" runat="server" visible="false">
                 <div class="col-md-1">
                    <div class="form-group">
                        <asp:Button ID="btnAdd" runat="server" Text="Add" class="btn btn-default" 
                            CommandName="Add"
                            CommandArgument="None"
                            OnCommand="CommandBtn_Click"/>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <button type="reset"  class="btn btn-default">Reset</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
