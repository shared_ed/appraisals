﻿using System;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Appraisals.Admin
{
    public partial class ObjectiveTypeNewWebForm : System.Web.UI.Page
    {
        protected class ObjectiveType
        {
            public String TypeName { get; set; }
            public Double IsFixed { get; set; }
            public Double MidYearReq { get; set; }
            public Double YearEndReq { get; set; }
            public Double IsRating { get; set; }
            public Double IsPerformance { get; set; }
            public Double IsComment { get; set; }
            public Double IsOpen { get; set; }
            public Double IsActive { get; set; }
            public Double IsSupportReq { get; set; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!Authorise.AuthoriseUser("ObjectiveType"))
                {
                    Server.Transfer("NotAuthorised.aspx", true);
                }
               
            }
        }

        protected void CommandBtn_Click(Object sender, CommandEventArgs e)
        {
            errors.InnerHtml = String.Empty;
            errors.Visible = false;
            messages.Visible = false;

            switch (e.CommandName)
            {
                case "Add":

                    Boolean success = true;

                    if(TypeName.Value.Length == 0)
                    {
                        errors.InnerHtml = "Please provide an 'Objective Type Name'.";
                        success = false;
                    }

                    if (ObjectiveTypeRank.Value.Length == 0)
                    {
                        if (errors.InnerHtml.Length > 0)
                        {
                            String newLine = errors.InnerHtml + "<br/>" + "Please provide an 'Objective Type Rank' for the new 'Objective Type'.";
                            errors.InnerHtml = newLine;
                        }
                        else
                        {
                            errors.InnerHtml = "Please provide an 'Objective Type Rank'.";
                        }
                        success = false;
                    }
                    else
                    {
                        Int32 parsedInt;
                        if (!Int32.TryParse(ObjectiveTypeRank.Value, out parsedInt))
                        {
                            
                            if (errors.InnerHtml.Length > 0)
                            {
                                String newLine = errors.InnerHtml + "<br/>" + "Please provide a valid integer value for 'Objective Type Rank'.";
                                errors.InnerHtml = newLine;
                            }
                            else
                            {
                                errors.InnerHtml = "Please provide a valid integer value for 'Objective Type Rank'.";
                            }
                            success = false;
                        }
                    }

                    if (success)
                    {
                        AddObjectiveType();
                    } 
                    else
                    {
                        errors.Visible = true;
                    }
                    break;

                default:
                    break;
            }
        }

        private void AddObjectiveType()
        {
            try
            {
                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_AddObjectiveType", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@TypeName", SqlDbType.VarChar).Value = TypeName.Value;
                        command.Parameters.Add("@IsFixed", SqlDbType.Bit).Value = IsFixed.Checked;
                        command.Parameters.Add("@MidYearReq", SqlDbType.Bit).Value = MidYearReq.Checked;
                        command.Parameters.Add("@YearEndReq", SqlDbType.Bit).Value = YearEndReq.Checked;
                        command.Parameters.Add("@IsRating", SqlDbType.Bit).Value = IsRating.Checked;
                        command.Parameters.Add("@IsPerformance", SqlDbType.Bit).Value = IsPerformance.Checked;
                        command.Parameters.Add("@IsComment", SqlDbType.Bit).Value = IsComment.Checked;
                        command.Parameters.Add("@IsOpen", SqlDbType.Bit).Value = IsOpen.Checked;
                        command.Parameters.Add("@IsActive", SqlDbType.Bit).Value = IsActive.Checked;
                        command.Parameters.Add("@IsSupportReq", SqlDbType.Bit).Value = IsSupportReq.Checked;
                        command.Parameters.Add("@ObjectiveTypeRank", SqlDbType.VarChar).Value = ObjectiveTypeRank.Value;

                        SQLConnection.Open();

                        try
                        {
                            int updateCount = command.ExecuteNonQuery();

                            if (updateCount == 0)
                            {
                                errors.InnerHtml = "Unable to add new 'Objective Type'. Sorry, no further details are available.";
                                errors.Visible = true;
                                messages.Visible = false;
                            }
                            else
                            {
                                messages.InnerHtml = "The new 'Objective Type' has been successfully added.";
                                messages.Visible = true;
                            }
                         }
                        catch (Exception ex)
                        {
                            errors.InnerHtml = "An error occurred while attempting to add a new 'Objective Typ'e: " + ex.Message;
                            errors.Visible = true;
                            messages.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(AddObjectiveType) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(AddObjectiveType) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }
    }
}