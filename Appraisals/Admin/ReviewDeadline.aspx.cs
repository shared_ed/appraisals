﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Web.UI.WebControls;

namespace Appraisals.Admin
{
    public partial class ReviewDeadlineWebForm : System.Web.UI.Page
    {
        protected class ReviewDeadline
        {
            public String DeadlineID { get; set; }
            public String AcademicYearID { get; set; }
            public String ReviewPeriodID { get; set; }
            public String DateFrom { get; set; }
            public String DateTo { get; set; }
        }

        private String AcademicYear;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!Authorise.AuthoriseUser("ReviewDeadline"))
                {
                    Server.Transfer("NotAuthorised.aspx", true);
                }
            }
            else
            {
                AcademicYear = academicYearList.SelectedItem.Value;
            }
        }

        protected void Year_Change(Object sender, EventArgs e)
        {
            AcademicYear = academicYearList.SelectedItem.Value;
            if (AcademicYear == "Not Selected")
            {
                reviewDeadlineRow.Visible = false;
            }
            else
            {
                GetReviewDeadline();

                reviewDeadlineRow.Visible = true;
            }
        }

        protected void GetReviewDeadline()
        {
            try
            {
                List<ReviewDeadline> ReviewDeadlineList = new List<ReviewDeadline>();

                addRow.Visible = false;
                saveRow.Visible = false;

                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_GetReviewDeadlines", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@AcademicYearID", SqlDbType.VarChar).Value = AcademicYear;

                        SQLConnection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (!reader.HasRows)
                            {
                                messages.InnerHtml = "There are currently no Review Deadlines set for the selected Academic Year.<br/>Please complete the 'Date From' & 'Date To' columns in the 'Review Deadlines' table.";
                                messages.Visible = true;

                                //add initial rows - effectively an insert
                                ReviewDeadlineList.Add(new ReviewDeadline()
                                {
                                    DeadlineID = "Assigned by system",
                                    AcademicYearID = AcademicYear,
                                    ReviewPeriodID = "Mid-Year",
                                    DateFrom = String.Empty,
                                    DateTo = String.Empty,
                                });

                                ReviewDeadlineList.Add(new ReviewDeadline()
                                {
                                    DeadlineID = "Assigned by system",
                                    AcademicYearID = AcademicYear,
                                    ReviewPeriodID = "End of Year",
                                    DateFrom = String.Empty,
                                    DateTo = String.Empty,
                                });

                                addRow.Visible = true;
                            }
                            else
                            {
                                while (reader.Read())
                                    ReviewDeadlineList.Add(new ReviewDeadline()
                                    {
                                        DeadlineID = reader["DeadlineID"].ToString(),
                                        AcademicYearID = reader["AcademicYearID"].ToString(),
                                        ReviewPeriodID = reader["ReviewPeriodID"].ToString(),
                                        DateFrom = reader["DateFrom"].ToString(),
                                        DateTo = reader["DateTo"].ToString(),
                                    });

                                messages.InnerHtml = "Please amend, as required, the 'Date From' & 'Date To' columns in the 'Review Deadlines' table.";
                                messages.Visible = true;

                                saveRow.Visible = true;
                            }
                        }
                    }
                }
                reviewDeadline.DataSource = ReviewDeadlineList;
                reviewDeadline.DataBind();
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(GetAdmins) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(GetAdmins) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }

        protected void CommandBtn_Click(Object sender, CommandEventArgs e)
        {
            errors.InnerHtml = String.Empty;
            errors.Visible = false;
            messages.Visible = false;

            Boolean success = true;
            TextBox txtBox;

            DateTime parsedMidYearFromDate;
            txtBox = (TextBox)reviewDeadline.Items[0].FindControl("DateFrom");
            if (!DateTime.TryParse(txtBox.Text, out parsedMidYearFromDate))
            {
                errors.InnerHtml = "Please provide a valid date for Mid-Year 'Date From'.";
                success = false;
            }

            DateTime parsedMidYearToDate;
            txtBox = (TextBox)reviewDeadline.Items[0].FindControl("DateTo");
            if (!DateTime.TryParse(txtBox.Text, out parsedMidYearToDate))
            {
                if (errors.InnerHtml.Length > 0)
                {
                    errors.InnerHtml = errors.InnerHtml + "<br/>" + "Please provide a valid date for Mid-Year 'Date To'.";
                }
                else
                {
                    errors.InnerHtml = "Please provide a valid date for Mid-Year 'Date To'.";
                }
                success = false;
            }


            DateTime parsedYearEndFromDate;
            txtBox = (TextBox)reviewDeadline.Items[1].FindControl("DateFrom");
            if (!DateTime.TryParse(txtBox.Text, out parsedYearEndFromDate))
            {
                if (errors.InnerHtml.Length > 0)
                {
                    errors.InnerHtml = errors.InnerHtml + "<br/>" + "Please provide a valid date for Year-End 'Date From'.";
                }
                else
                {
                    errors.InnerHtml = "Please provide a valid date for Year-End 'Date From'.";
                }
                success = false;
            }

            DateTime parsedYearEndToDate;
            txtBox = (TextBox)reviewDeadline.Items[1].FindControl("DateTo");
            if (!DateTime.TryParse(txtBox.Text, out parsedYearEndToDate))
            {
                if (errors.InnerHtml.Length > 0)
                {
                    errors.InnerHtml = errors.InnerHtml + "<br/>" + "Please provide a valid date for Year-End 'Date To'.";
                }
                else
                {
                    errors.InnerHtml = "Please provide a valid date for Year-End 'Date To'.";
                }
                success = false;
            }

            if (success)
            {
                if (parsedMidYearFromDate > parsedMidYearToDate)
                {
                    if (errors.InnerHtml.Length > 0)
                    {
                        errors.InnerHtml = errors.InnerHtml + "<br/>" + "Please ensure that the Mid-Year 'Date To' is later than 'Date From''.";
                    }
                    else
                    {
                        errors.InnerHtml = "Please ensure that the Mid-Year 'Date To' is later than 'Date From''.";
                    }
                    success = false;
                }
          
                if (parsedYearEndFromDate > parsedYearEndToDate)
                {
                    if (errors.InnerHtml.Length > 0)
                    {
                        errors.InnerHtml = errors.InnerHtml + "<br/>" + "Please ensure that the Year-End 'Date To' is later than 'Date From''.";
                    }
                    else
                    {
                        errors.InnerHtml = "Please ensure that the Year-End 'Date To' is later than 'Date From''.";
                    }
                    success = false;
                }
            }

            switch (e.CommandName)
            {
                case "Add":
                    if (success)
                    {
                        AddDateRanges(AcademicYear, "Mid-Year", parsedMidYearFromDate, parsedMidYearToDate);
                        AddDateRanges(AcademicYear, "End of Year", parsedYearEndFromDate, parsedYearEndToDate);
                    }
                    else
                    {
                        errors.Visible = true;
                    }

                    break;

                case "Save":

                    if (success)
                    {
                        txtBox = (TextBox)reviewDeadline.Items[0].FindControl("DeadlineID");
                        Int32 parsedMidYearDeadlineID = Int32.Parse(txtBox.Text);
                        txtBox = (TextBox)reviewDeadline.Items[1].FindControl("DeadlineID");
                        Int32 parsedYearEndDeadlineID = Int32.Parse(txtBox.Text);

                        UpdateDateRanges(parsedMidYearDeadlineID, "Mid-Year", parsedMidYearFromDate, parsedMidYearToDate);
                        UpdateDateRanges(parsedYearEndDeadlineID, "End of Year", parsedYearEndFromDate, parsedYearEndToDate);
                    }
                    else
                    {
                        errors.Visible = true;
                    }

                    break;

                default:
                    break;
            }
        }

        private void AddDateRanges(String AcademicYearID, string ReviewPeriodID, DateTime DateFrom, DateTime DateTo)
        {
            try
            {
                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("[sp_AddReviewDeadlines]", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@AcademicYearID", SqlDbType.VarChar).Value = AcademicYearID;
                        command.Parameters.Add("@ReviewPeriodID", SqlDbType.VarChar).Value = ReviewPeriodID;
                        command.Parameters.Add("@DateFrom", SqlDbType.DateTime).Value = DateFrom;
                        command.Parameters.Add("@DateTo", SqlDbType.DateTime).Value = DateTo;


                        SQLConnection.Open();

                        try
                        {
                            int updateCount = command.ExecuteNonQuery();

                            if (updateCount == 0)
                            {
                                errors.InnerHtml = "Unable to add "  + ReviewPeriodID + " Review Deadline. Sorry, no further details are available.";
                                errors.Visible = true;
                                messages.Visible = false;
                            }
                            else
                            {
                                messages.InnerText = "Your 'Review Deadline' changes have been saved.";
                                messages.Visible = true;
                                errors.Visible = false;
                            }
                        }
                        catch (Exception ex)
                        {
                            errors.InnerHtml = "An error occurred while attempting to save your changes: " + ex.Message;
                            errors.Visible = true;
                            messages.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(AddDateRanges) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(AddDateRanges) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }


        private void UpdateDateRanges(Int32 DeadlineID, string ReviewPeriodID, DateTime DateFrom, DateTime DateTo)
        {
            try
            {
                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("[sp_UpdateReviewDeadlines]", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@DeadlineID", SqlDbType.Int).Value = DeadlineID;
                        command.Parameters.Add("@DateFrom", SqlDbType.DateTime).Value = DateFrom;
                        command.Parameters.Add("@DateTo", SqlDbType.DateTime).Value = DateTo;

                        SQLConnection.Open();

                        try
                        {
                            int updateCount = command.ExecuteNonQuery();

                            if (updateCount == 0)
                            {
                                errors.InnerHtml = "Unable to update " + ReviewPeriodID + " Review Deadline. Sorry, no further details are available.";
                                errors.Visible = true;
                                messages.Visible = false;
                            }
                            else
                            {
                                messages.InnerText = "Your 'Review Deadline' changes have been saved.";
                                messages.Visible = true;
                                errors.Visible = false;
                            }
                        }
                        catch (Exception ex)
                        {
                            errors.InnerHtml = "An error occurred while attempting to save your changes: " + ex.Message;
                            errors.Visible = true;
                            messages.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(UpdateDateRanges) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(UpdateDateRanges) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }
    }
}