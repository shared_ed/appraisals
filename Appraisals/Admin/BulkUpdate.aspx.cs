﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Appraisals.Admin
{
    public partial class BulkUpdateWebForm : System.Web.UI.Page
    {
        protected class BulkData
        {
            public Int32 StaffTypeID { get; set; }
            public String StaffTypeDescription { get; set; }
            public Int32 ObjectiveTypeID { get; set; }
            public String TypeName { get; set; }
        }

        protected class StaffType
        {
            public Int32 StaffTypeID { get; set; }
            public String StaffTypeDescription { get; set; }
        }

        protected class ObjectiveType
        {
            public Int32 ObjectiveTypeID { get; set; }
            public String TypeName { get; set; }
        }

        List<BulkData> BulkDataList;

        protected void Page_Load(object sender, EventArgs e)
        {
            errors.Visible = false;
            errors.InnerHtml = String.Empty;
            messages.Visible = false;
            messages.InnerHtml = String.Empty;

            if (!IsPostBack)
            {
                if (!Authorise.AuthoriseUser("BulkUpdate"))
                {
                    Server.Transfer("NotAuthorised.aspx", true);
                }

                GetBulkData();

                // Use bulk data to populate the staff type dropdown
                var StaffTypes = BulkDataList.GroupBy(x => x.StaffTypeID).Select(g => g.First());
                staffTypeDropDown.Items.Add(new ListItem("Please select...", "Not Selected"));
                foreach (BulkData st in StaffTypes)
                {
                    staffTypeDropDown.Items.Add(new ListItem(st.StaffTypeDescription.ToString(), st.StaffTypeID.ToString()));
                }
            }
        }


        private void GetBulkData()
        {
            try
            {
               BulkDataList = new List<BulkData>();
                 
                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_getBulkUpdateData", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        SQLConnection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())

                        {
                            if (!reader.HasRows)
                            {
                                errors.InnerHtml = "No 'Staff Type' / 'ObjectiveType' combinations were eligible for Bulk Update.";
                                errors.Visible = true;
                                return;
                            }
                            else
                            {
                                while (reader.Read())
                                    BulkDataList.Add(new BulkData()
                                    {
                                        StaffTypeID = (Int32)reader["StaffTypeID"],
                                        StaffTypeDescription = reader["StaffTypeDescription"].ToString(),
                                        ObjectiveTypeID = (Int32)reader["ObjectiveTypeID"],
                                        TypeName = reader["TypeName"].ToString()
                                    });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(GetBulkData) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(GetBulkData) : " + ex.Message;
                }
                errors.Visible = true;
            }
        }

        protected void staffTypeDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            objectiveTypeDropDown.Items.Clear();

            if (staffTypeDropDown.SelectedValue == "Not Selected")
            {
                objectiveTypeDropDown.Enabled = false;
               
            }
            else
            {
                objectiveTypeDropDown.Enabled = true;

                GetBulkData();

                var ObjectiveTypes = BulkDataList.Where(s => s.StaffTypeDescription == staffTypeDropDown.SelectedItem.ToString()).ToList();

                //objectiveTypeDropDown.Items.Add(new ListItem("Please select...", "Not Selected"));
                foreach (BulkData ot in ObjectiveTypes)
                {
                    objectiveTypeDropDown.Items.Add(new ListItem(ot.TypeName.ToString(), ot.ObjectiveTypeID.ToString()));
                }
            }
        }

        protected void CommandBtn_Click(Object sender, CommandEventArgs e)
        {
            errors.InnerHtml = String.Empty;
            errors.Visible = false;
            messages.Visible = false;

            switch (e.CommandName)
            {
                case "Update":

                    if (academicYearDropDown.SelectedValue == "Not Selected")
                    {
                        errors.InnerHtml = "Please select an 'Academic Year'.";
                    }

                    if (staffTypeDropDown.SelectedValue == "Not Selected")
                    {
                        if (errors.InnerHtml.Length > 0)
                        {
                            String newLine = errors.InnerHtml + "<br/>";
                            errors.InnerHtml = newLine + "Please select both a 'Staff Type' & an 'Objective Type'.";
                        }
                        else
                        {
                            errors.InnerHtml = "Please select both a 'Staff Type' & an 'Objective Type'.";
                        }
                    }

                    if (errors.InnerHtml.Length > 0)
                    {
                        errors.Visible = true;
                    }
                    else
                    {
                        UpdateBulkData();
                    }

                    break;

                default:
                    break;
            }
        }


        private void UpdateBulkData()
        {
            try
            {
                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("[sp_BulkAddFixedObjectives]", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@AcademicYear", SqlDbType.VarChar).Value = academicYearDropDown.SelectedValue;
                        command.Parameters.Add("@ObjectiveTypeID", SqlDbType.Int).Value = Int32.Parse(objectiveTypeDropDown.SelectedValue);
                        command.Parameters.Add("@StaffTypeID", SqlDbType.Int).Value = Int32.Parse(staffTypeDropDown.SelectedValue);

                        String username = System.Web.HttpContext.Current.User.Identity.Name;
                        username = username.Substring(username.LastIndexOf('\\') + 1);
                        command.Parameters.Add("@Username", SqlDbType.VarChar).Value = username;
                       
                        SQLConnection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (!reader.HasRows)
                            {
                                errors.InnerHtml = "Unable to update database. Sorry, no further details are available.";
                                errors.Visible = true;
                                return;
                            }
                            else
                            {
                                while (reader.Read())
                                {
                                    string created = reader["NewObjectivesCreated"].ToString();
                                    string found = reader["ExistingObjectivesFound"].ToString();
                                    messages.InnerHtml = "Bulk update successful: " + created + " new objectives created, " + found + " existing objectives found.";
                                    messages.Visible = true;
                                };
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(UpdateBulkData) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(UpdateBulkData) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }

    }
}