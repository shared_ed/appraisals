﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Site.Master" AutoEventWireup="true" CodeBehind="FixedObjectives.aspx.cs" Inherits="Appraisals.Admin.FixedObjectivesWebForm" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="well well-sm" style="text-align:center">
        <h1 id="objTypeTitle" runat="server">Maintain Fixed Objectives</h1>
    </div>

    <div id="messages" class="alert alert-success alert-dismissible" runat="server" visible="false">
    </div>
    <div id="errors" class="alert alert-danger alert-dismissible" runat="server" visible="false">
    </div>
    
    <div class="panel panel-default">
        <div class="panel-heading">Choose an Academic Year that the Fixed Objectives will apply to</div>
        <div class="panel-body">
            <div class="row"  runat="server">
                <div class="col-md-12">
                        <div class="form-group">
                        <label for="academicYearDropDown">Select an Academic Year.</label>
                        <asp:DropDownList AutoPostBack="true" ID="academicYearDropDown" runat="server" CssClass="form-control" 
                         OnSelectedIndexChanged="academicYearDropDown_SelectedIndexChanged" >
                                <asp:ListItem value="Not Selected" >Please Select...</asp:ListItem>
                                <asp:ListItem value="17/18" >17/18</asp:ListItem>
                                <asp:ListItem value="18/19" >18/19</asp:ListItem>
                                <asp:ListItem value="19/20" >19/20</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default" id="insertSection" runat="server" visible="false">
        <div class="panel-heading"><b>Add</b> a <i>new</i> Fixed Objective.</div>
        <div class="panel-body">

            <div class="row"  runat="server">
                <div class="col-md-12">
                        <div class="form-group">
                        <label for="StaffTypeDropDown">Select a Staff Type</label>
                        <asp:DropDownList AutoPostBack="false" ID="StaffTypeDropDown" runat="server" CssClass="form-control" />
                    </div>
                </div>
            </div>
 
            <div class="row"  runat="server">
                <div class="col-md-12">
                        <div class="form-group">
                        <label for="ObjectiveTypeDropDown">Select an Objective Type</label>
                        <asp:DropDownList AutoPostBack="false" ID="ObjectiveTypeDropDown" runat="server" CssClass="form-control" />
                    </div>
                </div>
            </div>

            <div class="row"  runat="server" style="margin-top:20px;">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Please provide a Title, Description & Ranking for the new Objective</label>
                    </div>
                </div>
            </div>
            <div class="row"  runat="server">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="ObjTitle">Objective Title</label>
                        <input type="text"  class="form-control" id="ObjTitle" runat="server">
                    </div>
                </div>
            </div>

            <div class="row"  runat="server">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="ObjDescription">Objective Description</label>
                        <textarea type="text" rows="10"  class="form-control" id="ObjDescription" runat="server" />
                    </div>
                </div>
            </div>

            <div class="row"  runat="server">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="ObjRank">Objective Ranking <i>(format: integer)</i></label>
                        <input type="text"  class="form-control" id="ObjRank" runat="server">
                    </div>
                </div>
            </div>

            <div class="row" >
                <div class="col-md-2">
                    <div class="form-group">
                        <asp:Button ID="btnAdd" runat="server" Text="Add" class="btn btn-default" 
                            CommandName="Add"
                            CommandArgument="None"
                            OnCommand="CommandBtn_Click"/>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="panel panel-default" id="amendSection" runat="server" visible="false">
        <div class="panel-heading"><b>Amend</b> an <i>existing</i> Fixed Objective.</div>
        <div class="panel-body">

        <div class="row" >
            <div class="col-md-2">
                <div class="form-group">
                    <asp:Button ID="btnSaveTop" runat="server" Text="Save" class="btn btn-default" 
                        CommandName="Save"
                        CommandArgument="None"
                        OnCommand="CommandBtn_Click"/>
                </div>
            </div>
        </div>

        <div class="row " >
            <div class="col-md-12">
                <table class="table">
                    <thead>
                        <tr>
                            <th style="max-width:50px;">Staff Type</th>
                            <th style="min-width:50px;">Objective Type</th>
                            <th style="min-width:250px;">Objective Title</th>
                            <th style="min-width:500px;">Objective Text</th>
                            <th style="max-width:10px;">Objective Rank</th>
                            <th style="display:none;">Original Title</th>
                            <th style="display:none;">Original Text</th>
                            <th style="display:none;">Original Rank</th>
                            <th style="display:none;">Original</th>
                            <th style="display:none;">ObjectiveTypeID</th>
                            <th style="display:none;">StaffTypeID</th>

                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="fixedObjectives" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td><asp:TextBox  class="form-control" readOnly="true" style="border:none;"  ID="StaffTypeDescription" runat="server" Text='<%# Eval("StaffTypeDescription") %>' /></td>
                                    <td><asp:TextBox  class="form-control" readOnly="true" style="border:none;"  ID="TypeName" runat="server" Text='<%# Eval("TypeName") %>' /></td>
                                    <td><asp:TextBox  class="form-control"  style="border:none;"  ID="ObjectiveTitle" runat="server" Text='<%# Eval("ObjectiveTitle") %>' /></td>
                                    <td><asp:TextBox  class="form-control"  TextMode="MultiLine" rows="4" style="border:none;min-width:500px"  ID="ObjectiveText" runat="server" Text='<%# Eval("ObjectiveText") %>' /></td>
                                    <td><asp:TextBox  class="form-control"  style="border:none;"  ID="ObjectiveRank" runat="server" Text='<%# Eval("ObjectiveRank") %>' /></td>
                                    <td style="display:none;"><asp:TextBox   ID="OriginalTitle" runat="server" Text='<%# Eval("OriginalTitle") %>' /></td>
                                    <td style="display:none;"><asp:TextBox   ID="OriginalText" runat="server" Text='<%# Eval("OriginalText") %>' /></td>
                                    <td style="display:none;"><asp:TextBox   ID="OriginalRank" runat="server" Text='<%# Eval("OriginalRank") %>' /></td>
                                    <td style="display:none;"><asp:TextBox   ID="FixedObjectiveID" runat="server" Text='<%# Eval("FixedObjectiveID") %>' /></td>
                                    <td style="display:none;"><asp:TextBox  ID="ObjectiveTypeID" runat="server" Text='<%# Eval("ObjectiveTypeID") %>' /></td>
                                    <td style="display:none;"><asp:TextBox  ID="StaffTypeID" runat="server" Text='<%# Eval("StaffTypeID") %>' /></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row" >
            <div class="col-md-2">
                <div class="form-group">
                    <asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-default" 
                        CommandName="Save"
                        CommandArgument="None"
                        OnCommand="CommandBtn_Click"/>
                </div>
            </div>
        </div>
    </div>
</div>

</asp:Content>
