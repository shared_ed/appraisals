﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Site.Master" AutoEventWireup="true" CodeBehind="ObjectiveTypeAmend.aspx.cs" Inherits="Appraisals.Admin.ObjectiveTypeAmendWebForm" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="well well-sm" style="text-align:center">
        <h1 id="objTypeTitle" runat="server">Objective Type Maintenance - Change Existing</h1>
    </div>

    <div style="padding-bottom:15px;"><a class="btn btn-default" href="ObjectiveTypeNew">&laquo; Add New</a></div>

    <div id="messages" class="alert alert-success alert-dismissible" runat="server" visible="false">
    </div>
    <div id="errors" class="alert alert-danger alert-dismissible" runat="server" visible="false">
    </div>
    
    <div class="panel panel-default">
        <div class="panel-heading">Objective Type attributes.</div>
        <div class="panel-body">

       
            <div class="row">
                    <div class="col-md-12">
                        <table class="table">
                        <thead>
                            <tr>
                                <th style="text-align:center;">Id</th>
                                <th style="text-align:center;min-width:300px;">Type<br/>Name</th>
                                <th >Is<br/>Fixed?</th>
                                <th >Required<br/>Mid-Year?</th>
                                <th >Required<br/>Year-End?</th>
                                <th>Is<br/>Rating?</th>
                                <th>Is<br/>Performance?</th>
                                <th>Is<br/>Comment?</th>
                                <th >Is<br/>SMART?</th>
                                <th>Is<br/>Active?</th>
                                <th >Is<br/>Support<br/>Required?</th>
                                <th style="text-align:center;" >Rank</th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater ID="objectiveTypes" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td><asp:TextBox ReadOnly="true" style="text-align:center;" class="form-control" ID="ObjectiveTypeID" runat="server" Text='<%# Eval("ObjectiveTypeID") %>' /></td>
                                        <td><asp:TextBox  class="form-control" ID="TypeName" runat="server" Text='<%# Eval("TypeName") %>' /></td>
                                        <td ><label class="checkbox-inline"><input style="text-align:end;" type="checkbox" id="IsFixed" runat="server" checked='<%# Eval("IsFixed") %>'></label></td>
                                        <td ><label class="checkbox-inline"><input style="text-align:end;" type="checkbox" id="MidYearReq" runat="server" checked='<%# Eval("MidYearReq") %>'></label></td>
                                        <td ><label class="checkbox-inline"><input style="text-align:end;" type="checkbox" id="YearEndReq" runat="server" checked='<%# Eval("YearEndReq") %>'></label></td>
                                        <td ><label class="checkbox-inline"><input style="text-align:end;" type="checkbox" id="IsRating" runat="server" checked='<%# Eval("IsRating") %>'></label></td>
                                        <td ><label class="checkbox-inline"><input style="text-align:end;" type="checkbox" id="IsPerformance" runat="server" checked='<%# Eval("IsPerformance") %>'></label></td>
                                        <td ><label class="checkbox-inline"><input style="text-align:end;" type="checkbox" id="IsComment" runat="server" checked='<%# Eval("IsComment") %>'></label></td>
                                        <td ><label class="checkbox-inline"><input style="text-align:end;" type="checkbox" id="IsOpen" runat="server" checked='<%# Eval("IsOpen") %>'></label></td>
                                        <td ><label class="checkbox-inline"><input style="text-align:end;" type="checkbox" id="IsActive" runat="server" checked='<%# Eval("IsActive") %>'></label></td>
                                        <td ><label class="checkbox-inline"><input style="text-align:end;" type="checkbox" id="IsSupportReq" runat="server" checked='<%# Eval("IsSupportReq") %>'></label></td>
                                        <td><asp:TextBox  style="text-align:center;" class="form-control" ID="ObjectiveTypeRank" runat="server" Text='<%# Eval("ObjectiveTypeRank") %>' /></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row" >
                <div class="col-md-1">
                    <div class="form-group">
                        <asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-default" style="margin-left:20px; "
                            CommandName="Save"
                            CommandArgument="None"
                            OnCommand="CommandBtn_Click"/>
                    </div>
                </div>
                    <div class="col-md-3">
                    <div class="form-group">
                    <button type="reset"  class="btn btn-default">Reset</button>
                </div>
            </div>
        </div>

        </div>
    </div>

</asp:Content>
