﻿<%@ Page Title="" Language="C#" MasterPageFile="Site.Master" AutoEventWireup="true" CodeBehind="BulkUpdate.aspx.cs" Inherits="Appraisals.Admin.BulkUpdateWebForm" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="well well-sm" style="text-align:center">
        <h1 id="objTypeTitle" runat="server">Bulk Competency/TLA creation</h1>
    </div>

    <div id="messages" class="alert alert-success alert-dismissible" runat="server" visible="false">
    </div>
    <div id="errors" class="alert alert-danger alert-dismissible" runat="server" visible="false">
    </div>
    
    <div class="panel panel-default">

        <div class="panel-heading">Please select an 'Academic Year', 'Staff Type' & 'Objective Type' for the Bulk Competency update.</div>

        <div class="panel-body">

            <div class="row"  runat="server">
                    <div class="col-md-12">
                    <div class="form-group">
                        <label for="academicYearDropDown">Academic Year</label>
                        <asp:DropDownList AutoPostBack="true" ID="academicYearDropDown" runat="server" CssClass="form-control" >
                                <asp:ListItem value="Not Selected" >Please Select...</asp:ListItem>
                                <asp:ListItem value="17/18" >17/18</asp:ListItem>
                                <asp:ListItem value="18/19" >18/19</asp:ListItem>
                                <asp:ListItem value="19/20" >19/20</asp:ListItem>
                                <asp:ListItem value="20/21" >19/20</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>

            <div class="row"  runat="server">
                <div class="col-md-4">
                        <div class="form-group">
                        <label for="staffTypeDropDown">Staff Type</label>
                        <asp:DropDownList AutoPostBack="true" ID="staffTypeDropDown" runat="server" CssClass="form-control" OnSelectedIndexChanged="staffTypeDropDown_SelectedIndexChanged"/>
                    </div>
                </div>

                <div class="col-md-8">
                        <div class="form-group">
                        <label for="staffTypeDropDown">Objective Type</label>
                        <asp:DropDownList AutoPostBack="true" ID="objectiveTypeDropDown" enabled="false" runat="server" CssClass="form-control" style="min-width:400px;"/>
                    </div>
                </div>
            </div>

            <div class="row"  runat="server">
                <div class="col-md-1">
                    <div class="form-group">
                        <asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-default" 
                            CommandName="Update"
                            CommandArgument="None"
                            OnCommand="CommandBtn_Click"/>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
