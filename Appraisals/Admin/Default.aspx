﻿<%@ Page Title="" Language="C#" MasterPageFile="Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Appraisals._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="well" style="text-align:center">
        <h1>HR Appraisals Administration</h1>
      
    </div>
    
    <div class="row">
        <div class="col-md-4">
            <h2>Academic Year</h2>
            <p>
                Add additional Academic Years.
            </p>
            <p>
                <a class="btn btn-default" href="AcademicYears">Select &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>User</h2>
            <p>
                Maintain the list of Users who have access to the HR Appraisals Administration application.
            </p>
            <p>
                <a class="btn btn-default" href="Users">Select &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Objective Type</h2>
            <p>
                Create and maintain Objective Types and their attributes.
            </p>
            <p>
                <a class="btn btn-default" href="ObjectiveTypeNew">Select &raquo;</a>
            </p>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-4">
            <h2>Rating</h2>
            <p>
                Configure Ratings and their definitions.
            </p>
            <p>
                <a class="btn btn-default" href="Rating">Select &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Review Deadline</h2>
            <p>
                Set review deadlines for an Academic Year.
            </p>
            <p>
                <a class="btn btn-default" href="ReviewDeadline">Select &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Staff Type</h2>
            <p>
                Define Staff role types.
            </p>
            <p>
                <a class="btn btn-default" href="StaffType">Select &raquo;</a>
            </p>
        </div>
     </div>

     <div class="row">
        <div class="col-md-4">
            <h2>Staff Structure</h2>
            <p>
                Maintain Staff reporting structure (add a new appraiser/appraisee link or change the 'active' date range of an existing link).
            </p>
            <p>
                <a class="btn btn-default" href="StaffStructure">Select &raquo;</a>
            </p>
        </div>
         <div class="col-md-4">
            <h2>Appraisal Reporting Index</h2>
            <p>
                Links to MI Appraisal Reports.
            </p>
            <p>
                <a class="btn btn-default" href="Reports">Select &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Maintain Fixed Objectives</h2>
            <p>
                Create & Amend Fixed Objectives.
            </p>
            <p>
                <a class="btn btn-default" href="FixedObjectives">Select &raquo;</a>
            </p>
        </div>
     </div>

     <div class="row">
        <div class="col-md-4">
            <h2>Assign Staff Type to Staff member</h2>
            <p>
                Define a Staff member's role (add or amend).
            </p>
            <p>
                <a class="btn btn-default" href="StaffToStaffType">Select &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Maintain Staff Type to Objective Type</h2>
            <p>
                Define the Objective Types assigned to a specific Staff Type.
            </p>
            <p>
                <a class="btn btn-default" href="StaffToObjective">Select &raquo;</a>
            </p>
        </div>
          <div class="col-md-4">
            <h2>Maintain 'Super Users'</h2>
            <p>
                Add, Delete or Amend a 'Super User'; these Users may be granted permission to use specific Console components.
            </p>
            <p>
                <a class="btn btn-default" href="SuperUsers">Select &raquo;</a>
            </p>
        </div>
    </div>

     <div class="row">
        <div class="col-md-4">
            <h2>Bulk Competency/TLA creation</h2>
            <p>
                Configure and run process to bulk-create Competency and TLA objectives.
            </p>
            <p>
                <a class="btn btn-default" href="BulkUpdate">Select &raquo;</a>
            </p>
        </div>
    </div>
</asp:Content>
