﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Site.Master" AutoEventWireup="true" CodeBehind="NotAuthorised.aspx.cs" Inherits="Appraisals.Admin.NotAuthorised" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
          <div class="jumbotron">
            <h2>Authorisation Failure</h2>      
            <p>You are not authorised to use the HR Appraisals Administration Console.</p>
          </div>
    </div>
</asp:Content>
