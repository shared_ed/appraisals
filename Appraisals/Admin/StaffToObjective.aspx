﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Site.Master" AutoEventWireup="true" CodeBehind="StaffToObjective.aspx.cs" Inherits="Appraisals.Admin.StaffToObjectiveWebForm" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="well well-sm" style="text-align:center">
        <h1 runat="server">Maintain mappings between Staff Type and Objective Type</h1>
    </div>

    <div id="messages" class="alert alert-success alert-dismissible" runat="server" visible="false">
    </div>
    <div id="errors" class="alert alert-danger alert-dismissible" runat="server" visible="false">
    </div>
    
    <div class="panel panel-default">
        <div class="panel-heading">'Staff Type' to 'Objective Type' mappings.</div>
        <div class="panel-body">


        <div class="row"  runat="server">
            <div class="col-md-12">
                    <div class="form-group">
                    <label for="staffTypeDropDown">Staff Type</label>
                    <asp:DropDownList AutoPostBack="true" ID="staffTypeDropDown" runat="server" CssClass="form-control" OnSelectedIndexChanged="staffTypeDropDown_SelectedIndexChanged"/>
                </div>
            </div>
        </div>

       <div class="row" id="objectiveTypesRow" runat="server" visible="false">
            <div class="col-md-4">
                <table class="table">
                <thead>
                    <tr>
                        <th style="text-align:left;">Objective Type</th>
                        <th style="display:none;"></th>
                        <th style="display:none;"></th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="objectiveTypes" runat="server">
                        <ItemTemplate>
                            <tr>
                                <td ><input style="text-align:end;" type="checkbox" id="ObjectiveTypeSelected" runat="server" checked='<%# Eval("ObjectiveTypeSelected") %>'>&nbsp;&nbsp;<b><%# Eval("TypeName") %></b></td>
                                <td style="display:none;"><input style="text-align:end;" type="checkbox" id="OriginallySelected" runat="server" checked='<%# Eval("OriginallySelected") %>'></td>
                                <td><asp:TextBox ReadOnly="true" style="display:none;" class="form-control" ID="ObjectiveTypeID" runat="server" Text='<%# Eval("ObjectiveTypeID") %>' /></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row" id="buttonRow" runat="server" visible="false">
        <div class="col-md-1">
            <div class="form-group">
                <asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-default" style="margin-left:20px; "
                    CommandName="Save"
                    CommandArgument="None"
                    OnCommand="CommandBtn_Click"/>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <button type="reset"  class="btn btn-default">Reset</button>
            </div>
            </div>
        </div>
    </div>
</div>
</asp:Content>