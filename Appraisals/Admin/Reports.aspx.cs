﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Appraisals.Admin
{
    public partial class ReportsWebForm : System.Web.UI.Page
    {
        protected class Report
        {
            public String title { get; set; }
            public String description { get; set; }
            public String link { get; set; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!Authorise.AuthoriseUser("Reports"))
                {
                    Server.Transfer("NotAuthorised.aspx", true);
                }
                GetReports();
            }
        }

        private void GetReports()
        {
            try
            {
                List<Report> reportList = new List<Report>();

                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_GetReports", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        SQLConnection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (!reader.HasRows)
                            {
                                messages.InnerHtml = "There are currently no Appraisal Reports available.";
                                messages.Visible = true;
                                return;
                            }
                            else
                            {
                                while (reader.Read())
                                    reportList.Add(new Report()
                                    {
                                        title = reader["Title"].ToString(),
                                        description = reader["Description"].ToString(),
                                        link = "<a href='" + reader["URL"].ToString() + "'>View</a>",
                                    });
                            }
                        }
                    }
                }
                reports.DataSource = reportList;
                reports.DataBind();
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(GetReports) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(GetReports) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }
    }
}