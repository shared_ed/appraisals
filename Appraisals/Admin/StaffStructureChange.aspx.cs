﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Appraisals.Admin
{
    public class DateRange
    {
        public String DateFromNew { get; set; }
        public String DateFromOld { get; set; }
        public String DateToOld { get; set; }
        public String DateToNew { get; set; }
    }

    public partial class StaffStructureChangeWebForm : System.Web.UI.Page
    {
        private Int32 parsedMgrId;
        private Int32 parsedStaffId;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                errors.Visible = false;
                messages.Visible = false;
                staffDetailsSection.Visible = false;
            }
            else
            {
                if (!Authorise.AuthoriseUser("StaffStructure"))
                {
                    Server.Transfer("NotAuthorised.aspx", true);
                }
            }
        }

        protected void CommandBtn_Click(Object sender, CommandEventArgs e)
        {


            switch (e.CommandName)
            {

                case "Verify":
                    errors.InnerHtml = String.Empty;
                    errors.Visible = false;

                    Boolean success = true;

                    if (managerID.Value.Length == 0)
                    {
                        errors.InnerHtml = "Please provide a manager 'Staff ID'.";
                        success = false;
                    }
                    else
                    {
                        if (!Int32.TryParse(managerID.Value, out parsedMgrId))
                        {
                            if (errors.InnerHtml.Length > 0)
                            {
                                errors.InnerHtml = errors.InnerHtml + "<br/>" + "'Staff ID' of manager is incorrect. Please enter a numeric value.";
                            }
                            else
                            {
                                errors.InnerHtml = "'Staff ID' of manager is incorrect. Please enter a numeric value.";
                            }
                            success = false;
                        }
                    }

                    if (StaffID.Value.Length == 0)
                    {
                        if (errors.InnerHtml.Length > 0)
                        {
                            errors.InnerHtml = errors.InnerHtml + "<br/>" + "Please provide a 'Staff ID' for the colleague being appraised.";
                        }
                        else
                        {
                            errors.InnerHtml = "Please provide a 'Staff ID' for the colleague being appraised.";
                        }
                        success = false;
                    }
                    else
                    {
                        if (!Int32.TryParse(StaffID.Value, out parsedStaffId))
                        {
                            if (errors.InnerHtml.Length > 0)
                            {
                                errors.InnerHtml = errors.InnerHtml + "<br/>" + "'Staff ID' of employee is incorrect. Please enter a numeric value.";
                            }
                            else
                            {
                                errors.InnerHtml = "'Staff ID' of employee is incorrect. Please enter a numeric value.";
                            }
                            success = false;
                        }
                    }

                    if (!success) //failed validation
                    {
                        errors.Visible = true;
                        return;
                    }

                    //validation successful so crack on....
                    success = true;

                    managerDetails.Value = GetStaffDetails(parsedMgrId);

                    if (managerDetails.Value.Length == 0)
                    {
                        if (errors.InnerHtml.Length > 0)
                        {
                            errors.InnerHtml = errors.InnerHtml + "<br/>" + "Cannot find Staff details for 'Staff ID': " + parsedMgrId;
                        }
                        else
                        {
                            errors.InnerHtml = "Cannot find Staff details for 'Staff ID': " + parsedMgrId;
                        }
                        success = false;
                    }

                    staffDetails.Value = GetStaffDetails(parsedStaffId);
                    if (staffDetails.Value.Length == 0)
                    {
                        if (errors.InnerHtml.Length > 0)
                        {
                            errors.InnerHtml = errors.InnerHtml + "<br/>" + "Cannot find Staff details for 'Staff ID': " + parsedStaffId;
                        }
                        else
                        {
                            errors.InnerHtml = "Cannot find Staff details for 'Staff ID': " + parsedStaffId;
                        }
                        success = false;
                    }

                    if (success)
                    {
                        GetStaffStructure(parsedStaffId, parsedMgrId);

                        staffDetailsSection.Visible = true;
                        verifySection.Visible = false;
                        managerID.Attributes.Add("readonly", "readonly");
                        StaffID.Attributes.Add("readonly", "readonly");
                    }
                    else
                    {
                        errors.Visible = true;
                    }

                    break;

                case "Clear":
                    managerID.Value = String.Empty;
                    StaffID.Value = String.Empty;
                    staffDetails.Value = String.Empty;
                    staffDetailsSection.Visible = false;
                    verifySection.Visible = true;
                    managerID.Attributes.Remove("readonly");
                    StaffID.Attributes.Remove("readonly");
                    resetSection.Visible = false;
                    datesSection.Visible = false;
                    resetSection.Visible = false;

                    break;

                case "Add":

                    Int32.TryParse(managerID.Value, out parsedMgrId);
                    Int32.TryParse(StaffID.Value, out parsedStaffId);
                    ValidateAndUpdateStaffStructure(parsedStaffId, parsedMgrId);

                    break;

                default:
                    break;
            }
        }

        private String GetStaffDetails(Double staffID)
        {
            try
            {
                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_GetStaffDetails", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@StaffID", SqlDbType.Float).Value = staffID;

                        SQLConnection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {

                            while (reader.Read())

                                return reader["Forename"].ToString() + " " + reader["Surname"].ToString() + Environment.NewLine +
                                        reader["CurrentPost"].ToString() + Environment.NewLine +
                                        reader["OrganisationUnit"].ToString() + Environment.NewLine +
                                        reader["OrganisationDivision"].ToString();
                        }
                        return String.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(GetStaffDetails) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(GetStaffDetails) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
                return String.Empty; ;
            }
        }

        private void GetStaffStructure(Int32 staffID, Int32 mgrID)
        {
            try
            {
                List<DateRange> dateRangeList = new List<DateRange>();

                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_GetStaffStructureForMgrAndStaff", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@StaffID", SqlDbType.Float).Value = staffID;
                        command.Parameters.Add("@MgrID", SqlDbType.Float).Value = mgrID;

                        SQLConnection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (!reader.HasRows)
                            {
                                errors.InnerHtml = "There is no existing staff structure linking the specified Staff IDs; if required, please use the 'Add New' button.";
                                errors.Visible = true;
                                datesSection.Visible = false;
                                resetSection.Visible = true;
                                return;
                            }
                            else
                            {
                                while (reader.Read())
                                    dateRangeList.Add(new DateRange()
                                    {
                                        DateFromOld = reader["DateFrom"].ToString(),
                                        DateToOld = reader["DateTo"].ToString(),
                                        DateFromNew = reader["DateFrom"].ToString(),
                                        DateToNew = reader["DateTo"].ToString()
                                    });
                            }
                         }
                    }
                }
                dateRanges.DataSource = dateRangeList;
                dateRanges.DataBind();
                datesSection.Visible = true;
                resetSection.Visible = false;
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(GetStaffDetails) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(GetStaffDetails) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }

        private void ValidateAndUpdateStaffStructure(Int32 staffID, Int32 mgrID)
        {
            try
            {
                List<DateRange> dateRangeList = new List<DateRange>();
                Boolean success = true;
                errors.InnerHtml = String.Empty;

                foreach (RepeaterItem item in dateRanges.Items)
                {
                    if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                    {
                        var tbFromDateNew = (TextBox)item.FindControl("fromDateNew");
                        var tbToDateNew = (TextBox)item.FindControl("toDateNew");
                        var tbFromDateOld = (TextBox)item.FindControl("fromDateOld");
                        var tbToDateOld = (TextBox)item.FindControl("toDateOld");

                        DateTime validFromDate;
                        DateTime validToDate;

                        if (tbFromDateNew.Text != tbFromDateOld.Text || tbToDateNew.Text != tbToDateOld.Text)
                        {
                            DateRange updateDates = new DateRange();

                            if (tbFromDateNew.Text != tbFromDateOld.Text)
                            {
                                if (!DateTime.TryParse(tbFromDateNew.Text, out validFromDate))
                                {
                                    if (errors.InnerHtml.Length > 0)
                                    {
                                        errors.InnerHtml = errors.InnerHtml + "<br/>" + "'From Date' " + tbFromDateNew.Text + " is incorrect. Please enter a valid date in dd/mm/yyyy format.";
                                    }
                                    else
                                    {
                                        errors.InnerHtml = "'From Date' " + tbFromDateNew.Text + " is incorrect. Please enter a valid date in dd/mm/yyyy format.";
                                    }
                                    success = false;
                                }
                            }

                            if (tbToDateNew.Text != tbToDateOld.Text)
                            {
                                if (tbToDateNew.Text.Length != 0) //allow blank end date
                                {
                                    if (!DateTime.TryParse(tbToDateNew.Text, out validToDate))
                                    {
                                        if (errors.InnerHtml.Length > 0)
                                        {
                                            errors.InnerHtml = errors.InnerHtml + "<br/>" + "'To Date' " + tbToDateNew.Text + " is incorrect. Please enter a valid date in dd/mm/yyyy format.";
                                        }
                                        else
                                        {
                                            errors.InnerHtml = "'To Date' " + tbToDateNew.Text + " is incorrect. Please enter a valid date in dd/mm/yyyy format.";
                                        }
                                        success = false;
                                    }
                                }
                            }

                            if (success)
                            {
                                updateDates.DateFromOld = tbFromDateOld.Text;
                                updateDates.DateFromNew = tbFromDateNew.Text;
                                updateDates.DateToOld = tbToDateOld.Text;
                                updateDates.DateToNew = tbToDateNew.Text;
                                dateRangeList.Add(updateDates);
                            }
                        }
                    }
                }

                if (success)
                {
                    errors.Visible = false;
                    foreach (DateRange dates in dateRangeList)
                    {
                        UpdateStaffStructure(mgrID,
                                            staffID,
                                            dates.DateFromOld,
                                            dates.DateToOld,
                                            dates.DateFromNew,
                                            dates.DateToNew);
                    }
                }
                else
                {
                    errors.Visible = true;
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(GetStaffDetails) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(GetStaffDetails) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }

        private void UpdateStaffStructure(Int32 mgrId, 
                                            Int32 staffId, 
                                            String fromDateOld,
                                            String toDateOld,
                                            String fromDateNew,
                                            String toDateNew)
        {
            try
            {
                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("[sp_UpdateStaffStructureForMgrAndStaff]", SQLConnection))
                    {
                        DateTime anyOldDate;
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@StaffID", SqlDbType.Int).Value = staffId;

                        command.Parameters.Add("@MgrID", SqlDbType.Int).Value = mgrId;

                        DateTime.TryParse(fromDateOld, out anyOldDate);
                        command.Parameters.Add("@DateFromOld", SqlDbType.DateTime).Value = anyOldDate;

                        if (toDateOld.ToString().Length != 0)
                        {
                            DateTime.TryParse(toDateOld, out anyOldDate);
                            command.Parameters.Add("@DateToOld", SqlDbType.DateTime).Value = anyOldDate;
                        }
                        else
                        {
                            command.Parameters.Add("@DateToOld", SqlDbType.DateTime).Value = DBNull.Value;
                        }

                        DateTime.TryParse(fromDateNew, out anyOldDate);
                        command.Parameters.Add("@DateFromNew", SqlDbType.DateTime).Value = anyOldDate;

                        if (toDateNew.ToString().Length != 0 )
                        {
                            DateTime.TryParse(toDateNew, out anyOldDate);
                            command.Parameters.Add("@DateToNew", SqlDbType.DateTime).Value = anyOldDate;
                        }
                        else
                        {
                            command.Parameters.Add("@DateToNew", SqlDbType.DateTime).Value = DBNull.Value;
                        }

                        SQLConnection.Open();

                        try
                        {
                            int updateCount = command.ExecuteNonQuery();

                            if (updateCount > 0)
                            {
                                messages.InnerText = "Your 'Staff Structure' change has been saved.";
                                messages.Visible = true;
                                errors.Visible = false;
                            }
                            else
                            {
                                errors.InnerHtml = "Unable to save changes. Sorry, no further details are available.";
                                errors.Visible = true;
                                messages.Visible = false;
                            }
                        }
                        catch (Exception ex)
                        {
                            errors.InnerHtml = "An error occurred while attempting to save your changes: " + ex.Message;
                            errors.Visible = true;
                            messages.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(UpdateStaffStructure) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(UpdateStaffStructure) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }
    }
}