﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;

namespace Appraisals.Reviews
{

    public partial class WebForm3 : System.Web.UI.Page
    {
        private String StaffName;
        private String MgrName;
        private String AcademicYear; // = "17/18";
        private String ReviewPeriod; // = "ENDOFYEAR";
        private int StaffID = 200494; // = 100038; 200494;
        private String RequesterUserName; // = "MISFDS";

        private List<Comments> CommentsList;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                Boolean validRequest = true;

                if (Request.QueryString["AcademicYear"] != null)
                { AcademicYear = Request.QueryString["AcademicYear"]; }
                else { validRequest = false; }

                if (Request.QueryString["Term"] != null)
                { ReviewPeriod = Request.QueryString["Term"]; }
                else { validRequest = false; }

                if (Request.QueryString["StaffID"] != null)
                { StaffID = Int32.Parse(Request.QueryString["StaffID"]); }
                else { validRequest = false; }

                if (Request.QueryString["UserName"] != null)
                { RequesterUserName = Request.QueryString["UserName"]; }
                else { validRequest = false; }

                if (!validRequest)
                {
                    Server.Transfer("NotValid.aspx", true);
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(Page_Init) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(Page_Init) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                try
                {
                    Int32 reformattedCpdHours = 0;

                    if (cpdHoursRow.Visible == true)
                    {

                        if (!Int32.TryParse(cpdHours.Value, out reformattedCpdHours))
                        {
                            errors.InnerHtml = "'CPD Hours' must be a valid positive, integer .";
                            errors.Visible = true;
                            return;
                        }
                       
                        if (reformattedCpdHours <= 0)
                        {
                            errors.InnerHtml = "'CPD Hours' must be a valid positive, integer .";
                            errors.Visible = true;
                            return;
                           
                        }
                    }

                    Boolean successfulUpdate =
                        UpdateComments(TlaDropDownList.SelectedValue,
                                        ratingsDropDownList.SelectedValue,
                                        employeeAppraisalComment.Value,
                                        employeePerformanceComment.Value,
                                        managerComments.Value,
                                        employeeOpportunity.Checked,
                                        reviewComplete.Checked,
                                        cpdComments.Value,
                                        cpdInPlace.Checked,
                                        skillsQuestion.Checked,
                                        reformattedCpdHours,
                                        cpdRatingDropDownList.SelectedValue);

                    if (successfulUpdate) //change local stored data 
                    {
                      //not currently required
                    }
                }
                catch (Exception ex)
                {
                    if (errors.InnerHtml.Length > 0)
                    {
                        String newLine = errors.InnerHtml + "<br/>" + "(Page_Load/IsPostback) :";
                        errors.InnerHtml = newLine + ex.Message;
                    }
                    else
                    {
                        errors.InnerHtml = "(Page_Load/IsPostback) : " + ex.Message;
                    }
                    errors.Visible = true;
                    messages.Visible = false;
                }
            }
            else //First time in...
            {
                try
                {
                    Authorise();

                    CreateDropdowns();

                    GetCommentsData();

                    if (ReviewPeriod.ToUpper().Equals("ENDOFYEAR"))
                    {
                        objTypeTitle.InnerText = "End of Year Comments for " + StaffName;
                    }
                    else
                    {
                        if (ReviewPeriod.ToUpper().Equals("OBJECTIVESETTING"))
                        {
                            objTypeTitle.InnerText = "Start of Year Comments for " + StaffName;

                        }
                        else
                        {
                            objTypeTitle.InnerText = "Mid-Year Comments for " + StaffName;
                        }
                    }

                    commentID.Value = CommentsList[0].CommentID.ToString();
                    employeeOpportunity.Checked = CommentsList[0].EmployeeOpportunity;
                    employeeAppraisalComment.Value = CommentsList[0].EmployeeCommentAppraisal;
                    managerComments.Value = CommentsList[0].ManagerComment;
                    employeePerformanceComment.Value = CommentsList[0].EmployeeCommentPerformance;
                    TlaDropDownList.SelectedValue = CommentsList[0].TLARating.ToString();
                    ratingsDropDownList.SelectedValue = CommentsList[0].OverallRating.ToString();
                    reviewComplete.Checked = CommentsList[0].ReviewComplete;
                    cpdComments.Value = CommentsList[0].CPDComment;
                    cpdInPlace.Checked = CommentsList[0].CPDInPlace;
                    skillsQuestion.Checked = CommentsList[0].SkillsQuestionnaireComplete;
                    cpdHours.Value = CommentsList[0].CPDHours == 0 ? "" : CommentsList[0].CPDHours.ToString();
                    cpdRatingDropDownList.SelectedValue = CommentsList[0].CPDRating.ToString();

                    if (ReviewPeriod.ToUpper() == "OBJECTIVESETTING") //active at start of year
                    {
                        if (CommentsList[0].StaffType == 2 || CommentsList[0].StaffType == 6) //Academic staff or Heads of School
                        {
                            AcademicSection.Visible = true;
                            cpdInPlaceRow.Visible = true;
                            skillsQuestionRow.Visible = true;
                        }

                        defaultSection.Visible = true;
                        reviewCompleteRow.Visible = true;
                    }
                    else
                    {
                        if (ReviewPeriod.ToUpper() == "MID-YEAR")
                        {
                            if (CommentsList[0].StaffType == 2 || CommentsList[0].StaffType == 6) //Academic staff or Heads of School
                            {
                                AcademicSection.Visible = true;
                                cpdCommentsRow.Visible = true;
                                cpdHoursRow.Visible = true;
                                cpdRatingRow.Visible = true;
                            }

                            defaultSection.Visible = true;
                            reviewCompleteRow.Visible = true;
                        }
                        else // End of Year
                        {
                            if (CommentsList[0].StaffType == 2 || CommentsList[0].StaffType == 6) //Academic staff or Heads of School
                            {
                                AcademicSection.Visible = true;
                                cpdCommentsRow.Visible = true;
                                cpdHoursRow.Visible = true;
                                cpdRatingRow.Visible = true;

                                GetAcademicKPIs();
                                academicKPIsection.Visible = true;
                            }
                            
                            defaultSection.Visible = true;
                            reviewCompleteRow.Visible = true;
                            endOfYearSection.Visible = true;
                            reviewCompleteRow.Visible = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (errors.InnerHtml.Length > 0)
                    {
                        String newLine = errors.InnerHtml + "<br/>" + "(Page_Load/Entry) :";
                        errors.InnerHtml = newLine + ex.Message;
                    }
                    else
                    {
                        errors.InnerHtml = "(Page_Load/Entry) : " + ex.Message;
                    }
                    errors.Visible = true;
                    messages.Visible = false;
                }
            }
        }

        private Boolean UpdateComments(String tlaRating,
                                        String overallRating,
                                        String employeeAppraisalComment,
                                        String employeePerformanceComment,
                                        String managerComments,
                                        Boolean employeeOpportunity,
                                        Boolean reviewComplete,
                                        String cpdComment,
                                        Boolean cpdInPlace,
                                        Boolean skillsQuestionnaireComplete,
                                        Int32 cpdHours,
                                        String cpdRating)
        {

            Boolean successfulUpdate = false;

            try
            {
                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("[sp_AddComment]", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@AcademicYearID", SqlDbType.VarChar).Value = AcademicYear;
                       
                        String reformattedPeriod;
                        if (ReviewPeriod.ToUpper().Equals("ENDOFYEAR"))
                        {
                            reformattedPeriod = "End of Year";
                        }
                        else
                        {
                            if (ReviewPeriod.ToUpper().Equals("OBJECTIVESETTING"))
                            {
                                reformattedPeriod = "Objective Setting";

                            }
                            else
                            {
                                reformattedPeriod = "Mid-Year";
                            }
                        }
                        command.Parameters.Add("@ReviewPeriodID", SqlDbType.VarChar).Value = reformattedPeriod;

                        command.Parameters.Add("@StaffID", SqlDbType.Int).Value = StaffID;
                        command.Parameters.Add("@Username", SqlDbType.VarChar).Value = mgrUserName.Value;
                        command.Parameters.Add("@TLARating", SqlDbType.VarChar).Value = tlaRating;
                        command.Parameters.Add("@OverallRating", SqlDbType.VarChar).Value = overallRating;
                        command.Parameters.Add("@EmpCommApp", SqlDbType.VarChar).Value = employeeAppraisalComment;
                        command.Parameters.Add("@EmpCommPerf", SqlDbType.VarChar).Value = employeePerformanceComment;
                        command.Parameters.Add("@MgrComm", SqlDbType.VarChar).Value = managerComments;
                        command.Parameters.Add("@EmpOppComm", SqlDbType.Bit).Value = employeeOpportunity;
                        command.Parameters.Add("@ReviewComplete", SqlDbType.Bit).Value = reviewComplete;
                        command.Parameters.Add("@CPDComment", SqlDbType.VarChar).Value = cpdComment;
                        command.Parameters.Add("@CPDInPlace", SqlDbType.Bit).Value = cpdInPlace;
                        command.Parameters.Add("@SkillsQuestionnaireComplete", SqlDbType.Bit).Value = skillsQuestionnaireComplete;
                        command.Parameters.Add("@CPDHours", SqlDbType.Int).Value = cpdHours;
                        command.Parameters.Add("@CPDRating", SqlDbType.VarChar).Value = cpdRating;


                        SQLConnection.Open();

                        try
                        {
                            int updateCount = command.ExecuteNonQuery();

                            if (updateCount > 0)
                            {
                                messages.InnerText = "Your changes have been saved.";
                                messages.Visible = true;
                                errors.Visible = false;
                                successfulUpdate = true;
                            }
                            else
                            {
                                errors.InnerHtml = "Unable to save changes. Sorry, no further details are available.";
                                errors.Visible = true;
                                messages.Visible = false;
                            }
                        }
                        catch (Exception ex)
                        {
                            errors.InnerHtml = "An error occurred while attempting to save your changes: " + ex.Message;
                            errors.Visible = true;
                            messages.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(UpdateComments) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "An unexpected error has occurred (UpdateComments) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
            return successfulUpdate;

        }

        private void Authorise()
        {
            //String userName = Environment.UserName;
            String userName = System.Web.HttpContext.Current.User.Identity.Name;
            userName = userName.Substring(userName.LastIndexOf('\\') + 1);
            Boolean mgrFound = false;

            try
            {
                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_getManagerForPerson", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@StaffID", SqlDbType.VarChar).Value = StaffID;

                        SQLConnection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                                if (!reader.HasRows)
                                {
                                    Server.Transfer("NotAuthorised.aspx", true);
                                }
                                else
                                {

                                    StaffName = reader["Forename"].ToString() + " " + reader["Surname"].ToString();
                                    MgrName = reader["mgrForename"].ToString() + " " + reader["mgrSurname"].ToString();
                                    mgrUserName.Value = reader["mgrUsername"].ToString();

                                    if (userName == RequesterUserName && userName == mgrUserName.Value)
                                    {
                                        mgrFound = true; ;
                                    }
                                }
                        }
                        if (!mgrFound)
                        {
                            //Server.Transfer("NotAuthorised.aspx", true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(Authorise) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "An unexpected error has occurred (Authorise) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }

        private void GetCommentsData()
        {
            try
            {
                CommentsList = new List<Comments>();

                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_getCommentsForPerson", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@AcademicYear", SqlDbType.VarChar).Value = AcademicYear;
                       
                        String reformattedPeriod;
                        if (ReviewPeriod.ToUpper().Equals("ENDOFYEAR"))
                        {
                            reformattedPeriod = "End of Year";
                        }
                        else
                        {
                            if (ReviewPeriod.ToUpper().Equals("OBJECTIVESETTING"))
                            {
                                reformattedPeriod = "Objective Setting";

                            }
                            else
                            {
                                reformattedPeriod ="Mid-Year";
                            }
                        }
                        command.Parameters.Add("@ReviewPeriod", SqlDbType.VarChar).Value = reformattedPeriod;

                        command.Parameters.Add("@StaffID", SqlDbType.VarChar).Value = StaffID;

                        SQLConnection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (!reader.HasRows)
                            {
                                Server.Transfer("NoData.aspx", true);
                            }
                            while (reader.Read())
                            {
                                CommentsList.Add(new Comments()
                                {
                                    CommentID = reader["CommentID"] is DBNull ? 0 : (int)reader["CommentID"],
                                    ReviewPeriodID = reader["ReviewPeriodID"] is DBNull ? "" : reader["ReviewPeriodID"].ToString(),
                                    TLARating = reader["TLARating"] is DBNull ? "" : reader["TLARating"].ToString(),
                                    OverallRating = reader["OverallRating"] is DBNull ? "" : (string)reader["OverallRating"],
                                    EmployeeCommentAppraisal = reader["EmployeeCommentAppraisal"] is DBNull ? "" : (string)reader["EmployeeCommentAppraisal"],
                                    EmployeeCommentPerformance = reader["EmployeeCommentPerformance"] is DBNull ? "" : (string)reader["EmployeeCommentPerformance"],
                                    ManagerComment = reader["ManagerComment"] is DBNull ? "" : (string)reader["ManagerComment"],
                                    EmployeeOpportunity = reader["EmployeeOpportunity"] is DBNull ? false : reader.GetBoolean(reader.GetOrdinal("EmployeeOpportunity")),
                                    ReviewComplete = reader["ReviewComplete"] is DBNull ? false : reader.GetBoolean(reader.GetOrdinal("ReviewComplete")),
                                    CreatedBy = reader["CreatedBy"] is DBNull ? "" : (string)reader["CreatedBy"],
                                    CreatedDate = reader["CreatedDate"] is DBNull ? "" : reader["CreatedDate"].ToString(),
                                    ModifiedBy = reader["ModifiedBy"] is DBNull ? "" : (string)reader["ModifiedBy"],
                                    ModifiedDate = reader["ModifiedDate"] is DBNull ? "" : reader["ModifiedDate"].ToString(),
                                    StaffType = reader["StaffTypeID"] is DBNull ? 0 : (int)reader["StaffTypeID"],
                                    CPDComment = reader["CPDComment"] is DBNull ? "" : (string)reader["CPDComment"],
                                    CPDInPlace = reader["CPDInPlace"] is DBNull ? false : reader.GetBoolean(reader.GetOrdinal("CPDInPlace")),
                                    SkillsQuestionnaireComplete = reader["SkillsQuestionnaireComplete"] is DBNull ? false : reader.GetBoolean(reader.GetOrdinal("SkillsQuestionnaireComplete")),
                                    CPDHours = reader["CPDHours"] is DBNull ? 0 : (int)reader["CPDHours"],
                                    CPDRating = reader["CPDRating"] is DBNull ? "" : (string)reader["CPDRating"]
                                });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(GetCommentsData) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(GetCommentsData) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }

        private void CreateDropdowns()
        {
            try
            {
                cpdRatingDropDownList.Items.Add(new ListItem(String.Empty, String.Empty));
                ratingsDropDownList.Items.Add(new ListItem(String.Empty, String.Empty));
                TlaDropDownList.Items.Add(new ListItem(String.Empty, String.Empty));

                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_getRatings", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        SQLConnection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                cpdRatingDropDownList.Items.Add(new ListItem((string)reader["RatingDescription"].ToString(),
                                            (string)reader["RatingID"].ToString()));

                                ratingsDropDownList.Items.Add(new ListItem((string)reader["RatingDescription"].ToString(),
                                                                            (string)reader["RatingID"].ToString()));

                                TlaDropDownList.Items.Add(new ListItem((string)reader["RatingDescription"].ToString(),
                                                                            (string)reader["RatingID"].ToString()));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(CreateRatingsDropdown) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "An unexpected error has occurred (CreateRatingsDropdown) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }

        private void GetAcademicKPIs()
        {
            try
            {
                List<KPI> KPI_list = new List<KPI>();

                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_getKPIforAcademicStaffMember", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@AcademicYear", SqlDbType.VarChar).Value = AcademicYear;
                        command.Parameters.Add("@StaffID", SqlDbType.VarChar).Value = StaffID;

                        SQLConnection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                KPI_list.Add(new KPI()
                                {
                                    Title = reader["Title"].ToString(),
                                    TargetValue = reader["TargetValue"].ToString(),
                                    Actual = reader["Actual"].ToString(),
                                });
                            }
                        }
                    }
                }

                academicKPI.DataSource = KPI_list;
                academicKPI.DataBind();
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(GetAcademicKPIs) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "An unexpected error has occurred (GetAcademicKPIs) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }
    }
}