﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reviews/ReviewsMaster.Master" AutoEventWireup="true" CodeBehind="Comments.aspx.cs" Inherits="Appraisals.Reviews.WebForm3" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="well well-sm" style="text-align:center">
        <h1 id="objTypeTitle" runat="server">Employee and Manager Comments</h1>
    </div>

    <div id="messages" class="alert alert-success alert-dismissible" runat="server" visible="false" />
    <div id="errors" class="alert alert-danger alert-dismissible" runat="server" visible="false" />
    
    <div class="row">
        <div class="col-md-12">
            <h2  id="objSelected" runat="server">Comments</h2>
       </div>
    </div>

    <div class="panel panel-default">

        <div class="panel-heading">Comment Details</div>

        <div class="panel-body">

                <div id="AcademicSection" visible="false" runat="server">

                    <div class="row" id="cpdCommentsRow" runat="server">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="cpdComments">CPD Comments</label>
                                <textarea class="form-control" style="min-width: 86%" rows="5" id="cpdComments" runat="server"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row" id="cpdInPlaceRow" runat="server" visible="false">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="checkbox">
                                    <label for="cpdInPlace"><b>Is a CPD plan in place?</b></label>
                                    <asp:CheckBox id="cpdInPlace" runat="server"
                                        AutoPostBack="False"
                                        TextAlign="Right"/>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row" id="skillsQuestionRow" runat="server" visible="false">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="checkbox">
                                    <label for="skillsQuestion"><b>Has a skills questionnnaire been completed?</b> (<a href="https://set.et-foundation.co.uk/digital-assets/self-assessment/" id="skillsQuestionLink">SET Self-Assessment Tool</a>)</label>
                                    <asp:CheckBox id="skillsQuestion" runat="server"
                                        AutoPostBack="False"
                                        TextAlign="Right"/>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row" id="cpdHoursRow" visible="false" runat="server">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="cpdHours">CPD Hours</label>
                                <input type="text" class="form-control" id="cpdHours" runat="server" >
                            </div>
                        </div>
                    </div>

                     <div class="row" id="cpdRatingRow" visible="false"  runat="server">
                        <div class="col-md-12">
                                <div class="form-group">
                                <label for="cpdRatingDropDownList">CPD Rating</label>
                                <asp:DropDownList ID="cpdRatingDropDownList" runat="server" CssClass="form-control"/>
                            </div>
                        </div>
                    </div>

                </div>

                <div id="defaultSection" visible="false" runat="server">

                    <div class="row" id="employeeOpportunityRow"  runat="server">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="checkbox">
                                    <label for="employeeOpportunity"><b>Has the employee had the opportunity to comment?</b></label>
                                    <asp:CheckBox id="employeeOpportunity" runat="server"
                                        AutoPostBack="False"
                                        TextAlign="Right"/>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row" id="employeeAppraisalCommentRow"  runat="server">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="employeeAppraisalComment">Employee's comments on Appraisal</label>
                                <textarea class="form-control" style="min-width: 86%" rows="5" id="employeeAppraisalComment" runat="server"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row" id="managerCommentsRow"  runat="server">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="managerComments">Manager's comments</label>
                                <textarea class="form-control" style="min-width: 86%" rows="5" id="managerComments" runat="server"></textarea>
                            </div>
                        </div>
                    </div>

        
                </div>

                <div id="endOfYearSection" visible="false" runat="server">

                    <div class="row" id="employeePerformanceCommentRow"  runat="server">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="employeePerformanceComment">Employee's comments on Performance</label>
                                <textarea class="form-control" style="min-width: 86%" rows="5" id="employeePerformanceComment" runat="server"></textarea>
                            </div>
                        </div>
                    </div>

                    <div id="academicKPIsection" visible="false" runat="server" >
                        <div class="row">
                            <div class="col-md-12">
                                <label for="tblAcademicKPI">KPIs to consider when determining TLA Rating:</label>
                                <table id= "tblAcademicKPI" class="table-bordered" >
                                    <thead>
                                        <tr>
                                            <th>KPI</th>
                                            <th>Target %</th>
                                            <th>Actual %</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="academicKPI" runat="server" >
                                            <ItemTemplate>
                                                <tr>
                                                    <td><asp:TextBox enabled="false" ReadOnly="true"  BorderStyle="None" ID="title" runat="server" Text='<%# Eval("Title") %>' /></td>
                                                    <td><asp:TextBox enabled="false" style="text-align:center;" ReadOnly="true" BorderStyle="None" Columns="3" ID="target" runat="server" Text='<%# Eval("TargetValue") %>' /></td>
                                                    <td><asp:TextBox enabled="false" style="text-align:center;" ReadOnly="true" BorderStyle="None" Columns="3"  ID="actual" runat="server" Text='<%# Eval("Actual") %>' /></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </div>

                        </div>
       
                        <div class="row" id="TLArow"  runat="server" style="padding-top:15px;">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="TlaDropDownList">TLA Rating</label>
                                    <asp:DropDownList ID="TlaDropDownList" runat="server" CssClass="form-control"/>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row" id="ratingRow"  runat="server">
                        <div class="col-md-12">
                                <div class="form-group">
                                <label for="ratingsDropDownList">Overall Rating</label>
                                <asp:DropDownList ID="ratingsDropDownList" runat="server" CssClass="form-control"/>
                            </div>
                        </div>
                    </div>

                    </div>

 
                <div class="row" id="reviewCompleteRow" visible="false" runat="server">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="checkbox">
                                <label for="reviewComplete"><b>Is the review complete?</b></label>
                                <asp:CheckBox id="reviewComplete" runat="server"
                                    AutoPostBack="False"
                                    TextAlign="Right"/>
                            </div>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-default">Save</button>

        </div>
    </div>

    <asp:HiddenField ID="commentID" runat="server" />
    <asp:HiddenField ID="mgrUserName" runat="server" />

     <script type="text/javascript">

         $(document).ready(function () { //for initial doc load checks etc
         });

         function catchSubmit() //for pre-post checks
         {
             return true;
         }

    </script>
    
   

</asp:Content>

