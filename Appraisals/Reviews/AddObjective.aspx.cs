﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;

namespace Appraisals.Reviews
{
    public partial class WebForm4 : System.Web.UI.Page
    {
        private String StaffName;
        private String MgrName;
        private String AcademicYear;// = "17/18";
        private int StaffID;// = 100038;
        private String RequesterUserName;// = "humrdw";
        private int objectiveTypeID;
        private int fixedObjectiveID;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                Boolean validRequest = true;

                if (Request.QueryString["AcademicYear"] != null)
                { AcademicYear = Request.QueryString["AcademicYear"]; }
                else { validRequest = false; }

                if (Request.QueryString["StaffID"] != null)
                { StaffID = Int32.Parse(Request.QueryString["StaffID"]); }
                else { validRequest = false; }

                if (Request.QueryString["UserName"] != null)
                { RequesterUserName = Request.QueryString["UserName"]; }
                else { validRequest = false; }

                if (!validRequest)
                {
                    Server.Transfer("NotValid.aspx", true);
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(Page_Init) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(Page_Init) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                try
                {
                    errors.Visible = false;
                    messages.Visible = false;

                    Boolean doAdd = true;

                    //check that it isn't just a change on the dropdown list....
                    var targetID = Request.Form["__EVENTTARGET"];
                    if (targetID != null && targetID != string.Empty)
                    {
                        var targetControl = this.Page.FindControl(targetID);

                        if (targetControl.ClientID == "MainContent_objTypeDropDownList" || targetControl.ClientID == "MainContent_fixedObjTypeDropDownList")
                        {
                            doAdd = false;
                        }
                    }

                    if (doAdd)
                    {
                        //Input validation
                        DateTime parsedSetDate;
                        if (!DateTime.TryParse(setDate.Value, out parsedSetDate))
                        {
                            errors.InnerHtml = "'Date objective set' is incorrect. Please enter a valid date in dd/mm/yyyy format.";
                            errors.Visible = true;
                            messages.Visible = false;
                            return;
                        }

                        if (objTypeDropDownList.SelectedValue == "Not selected")
                        {
                            errors.InnerHtml = "Please select an 'Objective Type' from the dropdown list.";
                            errors.Visible = true;
                            messages.Visible = false;
                            return;
                        }
                        else
                        {
                            string[] objSettings;
                            objSettings = objTypeDropDownList.SelectedValue.Split('/');
                            objectiveTypeID = Int32.Parse(objSettings[0]);
                        }

                        if (fixedObjSection.Visible)
                        {
                            if (fixedObjTypeDropDownList.SelectedValue == "Not selected")
                            {
                                errors.InnerHtml = "Please select a 'Fixed Objective Title' from the dropdown list.";
                                errors.Visible = true;
                                messages.Visible = false;
                                return;
                            }
                            else
                            {
                                string[] objSettings;
                                objSettings = fixedObjTypeDropDownList.SelectedValue.Split('^');
                                fixedObjectiveID = Int32.Parse(objSettings[1]);
                             
                            }
                        }

                        if (objTitleRow.Visible == true)
                        {
                            if (objTitle.Value.Length == 0)
                            {
                                errors.InnerHtml = "Please provide a 'SMART Objective Title' (but 'SMART Objective Text' is optional).";
                                errors.Visible = true;
                                messages.Visible = false;
                                return;
                            }
                        }

                        Double parsedTarget = -1;
                        if (targetRow.Visible == true)
                        {
                            if (!Double.TryParse(target.Value, out parsedTarget))
                            {
                                errors.InnerHtml = "'Target' percentage is incorrect. Please enter a numeric value between 0 and 100.";
                                errors.Visible = true;
                                messages.Visible = false;
                                return;
                            }

                            if (parsedTarget < 0 || parsedTarget > 100)
                            {
                                errors.InnerHtml = "'Target' percentage is out of range. Please enter a numeric value between 0 and 100.";
                                errors.Visible = true;
                                messages.Visible = false;
                                return;
                            }
                        }

                        if(supportRequiredRow.Visible == true)
                        {
                            if (supportRequired.Value.Length == 0)
                            {
                                errors.InnerHtml = "Please provide details of 'Support requirements'.";
                                errors.Visible = true;
                                messages.Visible = false;
                                return;
                            }
                        }

                        Boolean successfulUpdate = AddObjective(objectiveTypeID,
                                                               parsedSetDate,
                                                               objTitle.Value,
                                                               objDescription.Value,
                                                               parsedTarget,
                                                               supportRequired.Value,
                                                               fixedObjectiveID);

                        if (successfulUpdate) //reset form values 
                        {
                            setDate.Value = DateTime.Now.ToShortDateString();

                            objTypeDropDownList.SelectedIndex = 0;

                            if (fixedObjTypeDropDownList.Visible == true)
                            {
                                fixedObjTypeDropDownList.SelectedIndex = 0;
                            }
                            fixedObjText.Value = String.Empty;
                            fixedObjSection.Visible = false;

                            objTitle.Value = String.Empty;
                            objDescription.Value = String.Empty;
                            openObjSection.Visible = false;

                            target.Value = String.Empty;
                            targetRow.Visible = false;

                            supportRequired.Value = String.Empty;
                            supportRequiredRow.Visible = false;

                            GetObjectivesSummary();
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (errors.InnerHtml.Length > 0)
                    {
                        String newLine = errors.InnerHtml + "<br/>" + "(Page_Load/IsPostback) :";
                        errors.InnerHtml = newLine + ex.Message;
                    }
                    else
                    {
                        errors.InnerHtml = "(Page_Load/IsPostback) : " + ex.Message;
                    }
                    errors.Visible = true;
                    messages.Visible = false;
                }
            }
            else //First time in...
            {
                try
                {
                    Authorise();

                    GetStaffType();

                    PopulateObjectiveTypeDropdown();

                    setDate.Value = DateTime.Now.ToShortDateString();

                    objTypeTitle.InnerText = "Add new objective for " + StaffName;

                    GetObjectivesSummary();
                }
                catch (Exception ex)
                {
                    if (errors.InnerHtml.Length > 0)
                    {
                        String newLine = errors.InnerHtml + "<br/>" + "(Page_Load/Entry) :";
                        errors.InnerHtml = newLine + ex.Message;
                    }
                    else
                    {
                        errors.InnerHtml = "(Page_Load/Entry) : " + ex.Message;
                    }
                    errors.Visible = true;
                    messages.Visible = false;
                }
            }
        }

        private void Authorise()
        {
            //String userName = Environment.UserName;
            String userName = System.Web.HttpContext.Current.User.Identity.Name;
            userName = userName.Substring(userName.LastIndexOf('\\') + 1);
            Boolean mgrFound = false;

            try
            {
                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_getManagerForPerson", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@StaffID", SqlDbType.VarChar).Value = StaffID;

                        SQLConnection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                                if (!reader.HasRows)
                                {
                                    Server.Transfer("NotAuthorised.aspx", true);
                                }
                                else
                                {

                                    StaffName = reader["Forename"].ToString() + " " + reader["Surname"].ToString();
                                    MgrName = reader["mgrForename"].ToString() + " " + reader["mgrSurname"].ToString();
                                    mgrUserName.Value = reader["mgrUsername"].ToString();

                                    if (userName == RequesterUserName && userName == mgrUserName.Value)
                                    {
                                        mgrFound = true; ;
                                    }
                                }
                        }
                        if (!mgrFound)
                        {
                            //Server.Transfer("NotAuthorised.aspx", true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(Authorise) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "An unexpected error has occurred (Authorise) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }

        private void GetStaffType()
        {
            try
            {
                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_GetStaffTypeForPerson", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@StaffID", SqlDbType.Int).Value = StaffID;

                        SQLConnection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            { 
                                while (reader.Read())
                                {
                                    staffTypeID.Value = reader["StaffTypeID"].ToString();
                                }
                            }
                            else
                            {
                                Server.Transfer("NoData.aspx", true);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(GetStaffType) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "An unexpected error has occurred (GetStaffType) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }

        private void PopulateObjectiveTypeDropdown()
        {
            try
            {
                objTypeDropDownList.Items.Add(new ListItem("Please select...", "Not selected"));

                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_GetObjectiveByStaffType", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@StaffTypeID", SqlDbType.Int).Value = staffTypeID.Value;

                        SQLConnection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                objTypeDropDownList.Items.Add(new ListItem((string)reader["TypeName"].ToString(),
                                                                            (string)reader["ObjectiveTypeID"].ToString()
                                                                            + "/" + (string)reader["IsFixed"].ToString()
                                                                            + "/" + (string)reader["IsSupportReq"].ToString()
                                                                            + "/" + (string)reader["IsOpen"].ToString()
                                                                            + "/" + (string)reader["IsPerformance"].ToString()));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(PopulateObjectiveTypeDropdown) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "An unexpected error has occurred (PopulateObjectiveTypeDropdown) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }

        private void PopulateFixedObjectiveTypeDropdown(int objectiveTypeID)
        {
            try
            {
                //first clear out last entries
                if (fixedObjTypeDropDownList.Items.Count > 0)
                {
                    fixedObjTypeDropDownList.Items.Clear();
                }

                //now add new entries
                fixedObjTypeDropDownList.Items.Add(new ListItem("Please select...", "Not selected"));

                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_GetFixedObjective", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@StaffTypeID", SqlDbType.Int).Value = staffTypeID.Value;
                        command.Parameters.Add("@ObjectiveTypeID", SqlDbType.Int).Value = objectiveTypeID;
                        command.Parameters.Add("@AcademicYearID", SqlDbType.VarChar).Value = AcademicYear;

                        SQLConnection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                fixedObjTypeDropDownList.Items.Add(new ListItem((string)reader["ObjectiveTitle"].ToString(),
                                                                            (string)reader["ObjectiveTypeID"].ToString()
                                                                            + "^" + (string)reader["FixedObjectiveID"].ToString()
                                                                            + "^" + (string)reader["ObjectiveText"].ToString()));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(PopulateFixedObjectiveTypeDropdown) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "An unexpected error has occurred (PopulateFixedObjectiveTypeDropdown) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }

        private Boolean AddObjective(Int32 objectiveTypeID,
                                   DateTime setDate,
                                   String objTitle,
                                   String objDescription,
                                   Double target,
                                   String supportRequired,
                                   Int32 fixedObjID)
        {
            Boolean successfulUpdate = false;

            try
            {
                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("[sp_AddObjective]", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@StaffID", SqlDbType.Int).Value = StaffID;
                        command.Parameters.Add("@StaffTypeID", SqlDbType.Int).Value = staffTypeID.Value;
                        command.Parameters.Add("@ObjectiveTypeID", SqlDbType.Int).Value = objectiveTypeID;
                        command.Parameters.Add("@AcademicYearID", SqlDbType.VarChar).Value = AcademicYear;
                        command.Parameters.Add("@Username", SqlDbType.VarChar).Value = RequesterUserName;
                        command.Parameters.Add("@SetDate", SqlDbType.DateTime).Value = setDate;
                        command.Parameters.Add("@ObjectiveTitle", SqlDbType.VarChar).Value = objTitle;
                        command.Parameters.Add("@ObjectiveText", SqlDbType.VarChar).Value = objDescription;

                        command.Parameters.Add("@TargetValue", SqlDbType.Float).Value = target;
                        command.Parameters.Add("@SupportRequirements", SqlDbType.VarChar).Value = supportRequired;
                        if (fixedObjID != 0)
                        {
                            command.Parameters.Add("@FixedObjectiveID", SqlDbType.Int).Value = fixedObjID;
                        }

                        SQLConnection.Open();

                        try
                        {
                            int updateCount = command.ExecuteNonQuery();

                            if (updateCount > 0)
                            {
                                messages.InnerText = "Your changes have been saved.";
                                messages.Visible = true;
                                errors.Visible = false;
                                successfulUpdate = true;
                            }
                            else
                            {
                                errors.InnerHtml = "Unable to save changes. Sorry, no further details are available.";
                                errors.Visible = true;
                                messages.Visible = false;
                            }
                        }
                        catch (Exception ex)
                        {
                            errors.InnerHtml = "An error occurred while attempting to save your changes: " + ex.Message;
                            errors.Visible = true;
                            messages.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(AddObjective) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "An unexpected error has occurred (AddObjective) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
            return successfulUpdate;

        }

        protected void objTypeDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] objSettings;
            string objectiveTypeID;
            string isFixed;
            string isSupportReq;
            string isOpen;
            string isPerformance;

            try
            {
                objSettings = objTypeDropDownList.SelectedValue.Split('/');
                objectiveTypeID = objSettings[0];
                isFixed = objSettings[1];
                isSupportReq = objSettings[2];
                isOpen = objSettings[3];
                isPerformance = objSettings[4];

                if (isFixed == "True")
                {
                    PopulateFixedObjectiveTypeDropdown(Int32.Parse(objectiveTypeID));
                    fixedObjText.Value = String.Empty;
                    fixedObjSection.Visible = true;
                }
                else
                {
                    fixedObjSection.Visible = false;
                }

                if (isOpen == "True")
                {
                    openObjSection.Visible = true;

                    if (isFixed == "True")
                    {
                        objTitleRow.Visible = false;
                    }
                    else
                    {
                        objTitleRow.Visible = true;
                    }
                }
                else
                {
                    openObjSection.Visible = false;
                }

                if (isSupportReq == "True")
                {
                    supportRequiredRow.Visible = true;
                }
                else
                {
                    supportRequiredRow.Visible = false;
                }

                if (isPerformance == "True")
                {
                    targetRow.Visible = true;
                }
                else
                {
                    targetRow.Visible = false;
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(objTypeDropDownList_SelectedIndexChanged) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "An unexpected error has occurred (objTypeDropDownList_SelectedIndexChanged) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
            return;
        }

        protected void fixedObjTypeDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] objSettings;
            objSettings = fixedObjTypeDropDownList.SelectedValue.Split('^');
            fixedObjText.Value = objSettings[2];
        }

        private void GetObjectivesSummary()
        {
            try
            {
                List<ObjectivesSummary> objectiveSummary = new List<ObjectivesSummary>();

                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_getObjectivesForPerson", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@AcademicYear", SqlDbType.VarChar).Value = AcademicYear;
                        command.Parameters.Add("@StaffID", SqlDbType.VarChar).Value = StaffID;

                        SQLConnection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                objectiveSummary.Add(new ObjectivesSummary()
                                {
                                    ObjectiveType = reader["ObjectiveType"].ToString(),
                                    CreatedDate = reader["CreatedDate"].ToString(),
                                    ObjTitle = reader["ObjTitle"].ToString(),
                                    ObjText = reader["ObjText"].ToString(),
                                });
                            }
                        }
                    }
                }

                objSummary.DataSource = objectiveSummary;
                objSummary.DataBind();
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(GetMidYearSummary) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "An unexpected error has occurred (GetMidYearSummary) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }
    }
}