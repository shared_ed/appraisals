﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reviews/ReviewsMaster.Master" AutoEventWireup="true" CodeBehind="AddObjective.aspx.cs" Inherits="Appraisals.Reviews.WebForm4" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="well well-sm" style="text-align:center">
        <h1 id="objTypeTitle" runat="server">Add a new objective</h1>
    </div>

    <div id="messages" class="alert alert-success alert-dismissible" runat="server" visible="false">
    </div>
    <div id="errors" class="alert alert-danger alert-dismissible" runat="server" visible="false">
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <h2  id="objSelected" runat="server"></h2>
       </div>
    </div>

    
    <div class="panel panel-default">
        <div class="panel-heading">Objective Details</div>
        <div class="panel-body">

            <div class="row">
                <div class="col-md-12">
                        <label style="padding-bottom:15px;font-size:large;">Please note that Competencies & TLAP objectives are automatically created by another process.</label>
                </div>
            </div>

            <div class="row" id="startDateRow">
                <div class="col-md-12">
                    
                    <div class="form-group">
                        <label for="setDate">Date objective set <i>(format: dd/mm/yyyy)</i></label>
                        <input type="text" class="form-control" id="setDate" runat="server">
                    </div>
                </div>
            </div>

            <div class="row" id="objTypeRow"  runat="server">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="objTypeDropDownList">Objective Type</label>
                        <asp:DropDownList ID="objTypeDropDownList" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="objTypeDropDownList_SelectedIndexChanged"/>
                    </div>
                </div>
            </div> 

            <div id="fixedObjSection"  runat="server" visible="false">
                <div  class="row" runat="server" >
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="fixedObjTypeDropDownList">Fixed Objective Title</label>
                            <asp:DropDownList ID="fixedObjTypeDropDownList" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="fixedObjTypeDropDownList_SelectedIndexChanged"/>
                            (The 'Objective Type' you have selected is a 'fixed' objective; now please select a 'Fixed Objective Title').
                        </div>
                    </div>

                     <div class="col-md-4">
                        <div class="form-group">
                            <label for="fixedObjText">Fixed Objective Text (if available)</label>
                            <%--<input type="text" class="form-control" id="fixedObjText" runat="server" >--%>
                             <textarea class="form-control" readonly rows="10" id="fixedObjText" runat="server"></textarea>
                        </div>
                    </div>
                </div>
            </div> 
            

            <div id="openObjSection" runat="server" visible="false">
                <div class="row" id="objTitleRow"  runat="server"  visible="false">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="objTitle">SMART Objective Title</label>
                            <input type="text" class="form-control" id="objTitle" runat="server" >
                        </div>
                    </div>
                </div>

                <div class="row" id="objDescriptionRow"  runat="server">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="objDescription">SMART Objective Text</label>
                            <textarea class="form-control" style="min-width: 86%" rows="5" id="objDescription" runat="server"></textarea>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" id="targetRow"  runat="server" visible="false">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="target">Target % <i>(format: number between 0 and 100)</i></label>
                        <input type="text" class="form-control" id="target" runat="server" >
                    </div>
                </div>
            </div>    

            <div class="row" id="supportRequiredRow"  runat="server" visible="false">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="supportRequired">Support requirements</label>
                        <textarea class="form-control" style="min-width: 86%" rows="5" id="supportRequired" runat="server"></textarea>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-default">Add</button>
            <button type="reset"  class="btn btn-default">Reset</button>
        </div>
    </div>

    <div class="panel panel-default" id="objectivesSummary" runat="server">
            <div class="panel-heading">Summary of existing objectives for Academic Year</div>
                 <div class="panel-body">   
                    <div class="row">
                        <div class="col-md-12">
                          <table class="table">
                            <thead>
                                <tr>
                                    <th>Objective Type</th>
                                    <th>Created date</th>
                                    <th>Title</th>
                                    <th>Text</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="objSummary" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td><asp:TextBox ReadOnly="true" Columns="20" BorderStyle="None" ID="ObjectiveType" runat="server" Text='<%# Eval("ObjectiveType") %>' /></td>
                                            <td><asp:TextBox ReadOnly="true" BorderStyle="None"  ID="CreatedDate" runat="server" Text='<%# Eval("CreatedDate") %>' /></td>
                                            <td><asp:TextBox ReadOnly="true" Columns="40" TextMode="MultiLine"  BorderStyle="None"  ID="ObjTitle" runat="server" Text='<%# Eval("ObjTitle") %>' /></td>
                                            <td><asp:TextBox ReadOnly="true" Columns="50"  TextMode="MultiLine" Height="60px" BorderStyle="None"   ID="ObjText" runat="server" Text='<%# Eval("ObjText") %>' /></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    <asp:HiddenField ID="mgrUserName" runat="server" />
    <asp:HiddenField ID="staffTypeID" runat="server" />

     <script type="text/javascript">

         $(document).ready(function () { //for initial doc load checks etc
         });

         function catchSubmit() //for pre-post checks
         {
             return true;
         }

         function CellAction(tdControl) {
            // //whitewash objectives table i.e. make everything 'unselected'
            // $('#MainContent_ObjectivesTable td').each(function (i, cell) {
            //     var cell = $(cell);
            //     $(cell).css('background-color', 'white');  
            // });
            // //and now highlight selected row
            //$(tdControl).css('background-color', 'orange');  

             try {
                 document.getElementById("MainContent_messages").style.display = 'none';
             } catch (err){};
             try {
                 document.getElementById("MainContent_errors").style.display = 'none';
             } catch (err) { };

            $("#MainContent_reviewDate").val($(tdControl).find("input[type='hidden']:eq(0)").val());
            $("#MainContent_RatingsDropDownList").val($(tdControl).find("input[type='hidden']:eq(1)").val());
            $("#MainContent_actual").val($(tdControl).find("input[type='hidden']:eq(2)").val());
            $("#MainContent_comment").text($(tdControl).find("input[type='hidden']:eq(3)").val());
            $("#MainContent_objSelected").html($(tdControl).find("input[type='hidden']:eq(4)").val());
            $("#MainContent_ranking").val($(tdControl).find("input[type='hidden']:eq(5)").val());
            $("#MainContent_objectiveID").val($(tdControl).find("input[type='hidden']:eq(6)").val());
            $("#MainContent_reviewPeriodID").val($(tdControl).find("input[type='hidden']:eq(7)").val());
            $("#MainContent_target").val($(tdControl).find("input[type='hidden']:eq(8)").val());
        }

    </script>
    
   

</asp:Content>