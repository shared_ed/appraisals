﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;

namespace Appraisals.Reviews
{

    public partial class WebForm1 : System.Web.UI.Page
    {
        private String StaffName;
        private String MgrName;
        private String MgrUserName;
        private String AcademicYear;// = "17/18";
        private int ObjectiveTypeID;// = 2;
        private String Term;// = "ENDOFYEAR";
        private int StaffID;// = 100038;//100017;//200536;// 100038;
        private String RequesterUserName; // = "MISFDS";// "humrdw"; //"MISFDS";

        private List<ObjectiveReview> ObjReviewList;

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                Boolean validRequest = true;

                if (Request.QueryString["AcademicYear"] != null)
                { AcademicYear = Request.QueryString["AcademicYear"]; }
                else { validRequest = false; }

                if (Request.QueryString["ObjectiveTypeID"] != null)
                { ObjectiveTypeID = Int32.Parse(Request.QueryString["ObjectiveTypeID"]); }
                else { validRequest = false; }

                if (Request.QueryString["Term"] != null)
                {
                    Term = Request.QueryString["Term"].ToUpper() == "ENDOFYEAR" ? "End of Year" : Request.QueryString["Term"];
                }
                else { validRequest = false; }

                if (Request.QueryString["StaffID"] != null)
                { StaffID = Int32.Parse(Request.QueryString["StaffID"]); }
                else { validRequest = false; }

                if (Request.QueryString["UserName"] != null)
                { RequesterUserName = Request.QueryString["UserName"]; }
                else { validRequest = false; }

                if (!validRequest)
                {
                    Server.Transfer("NotValid.aspx", true);
                }

                GetReviewData();

                CreateObjectivesTable();

                CreateRatingsDropdown();

                if (Term == "End of Year")
                {
                    midYearSummary.Visible = true;
                    GetMidYearSummary();
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(Page_Init) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(Page_Init) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                try
                {
                    //Restore dynamic date etc so screen looks as before 'submit'.
                    int cellIx = Int32.Parse(rowIndex.Value);

                    if (ObjReviewList[cellIx].IsFixed && ObjReviewList[cellIx].IsOpen)
                    { objSelected.InnerText = ObjReviewList[cellIx].FixedTitle; }
                    else
                    {
                        if (ObjReviewList[cellIx].IsFixed && !ObjReviewList[cellIx].IsOpen)
                        { objSelected.InnerText = ObjReviewList[cellIx].FixedTitle; }
                        else
                        {
                            if (ObjReviewList[cellIx].IsOpen && !ObjReviewList[cellIx].IsFixed)
                            { objSelected.InnerText = ObjReviewList[cellIx].OpenTitle; }
                        }
                    }


                    //Input validation
                    
                    DateTime parsedReviewDate;
                    if (!DateTime.TryParse(reviewDate.Value, out parsedReviewDate))
                    {
                        errors.InnerHtml = "'Review Date' is incorrect. Please enter a valid date in dd/mm/yyyy format.";
                        errors.Visible = true;
                        messages.Visible = false;
                        return;
                    }

                    Double parsedActual;
                    if (ObjReviewList[cellIx].IsPerformance)
                    {
                        
                        if (!Double.TryParse(actual.Value, out parsedActual))
                        {
                            errors.InnerHtml = "'Actual' mark is incorrect. Please enter a numeric value between 0 and 100.";
                            errors.Visible = true;
                            messages.Visible = false;
                            return;
                        }

                        if (!(parsedActual >= 0 && parsedActual <= 100))
                        {
                            errors.InnerHtml = "'Actual percentage' is incorrect. Please enter a numeric value between 0 and 100.";
                            errors.Visible = true;
                            messages.Visible = false;
                            return;
                        }
                    }

                    String username = System.Web.HttpContext.Current.User.Identity.Name;
                    username = username.Substring(username.LastIndexOf('\\') + 1);

                    Boolean successfulUpdate =
                        UpdateReview(Int32.Parse(objectiveID.Value),
                                    username,
                                    parsedReviewDate,
                                    RatingsDropDownList.SelectedValue,
                                    ObjReviewList[0].IsPerformance ? actual.Value : null,
                                    comment.Value,
                                    reviewPeriodID.Value);

                    if (successfulUpdate) //change local stored data 
                    {
                        GetReviewData();
                        CreateObjectivesTable();

                        reviewDate.Value = ObjReviewList[cellIx].ReviewDate.ToString();
                        if (ObjReviewList[cellIx].IsRating)
                        {
                            RatingsDropDownList.SelectedValue = ObjReviewList[cellIx].Rating;
                        }

                        if (ObjReviewList[cellIx].IsPerformance)
                        {
                            actual.Value = ObjReviewList[cellIx].Actual;
                        }

                        if (ObjReviewList[cellIx].IsComment)
                        {
                            comment.Value = ObjReviewList[cellIx].Comment;
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (errors.InnerHtml.Length > 0)
                    {
                        String newLine = errors.InnerHtml + "<br/>" + "(Page_Load/IsPostback) :";
                        errors.InnerHtml = newLine + ex.Message;
                    }
                    else
                    {
                        errors.InnerHtml = "(Page_Load/IsPostback) : " + ex.Message;
                    }
                    errors.Visible = true;
                    messages.Visible = false;
                }
            } 
            else //First time in...
            {
                try
                {
                    Authorise();

                    //Set initial values
                    objTypeTitle.InnerText = ObjReviewList[0].ReviewPeriodID + " " + ObjReviewList[0].TypeName + " Review";
                    
                    objTypeSummary.InnerText = ObjReviewList[0].TypeName + " Review for " + StaffName;

                   // reviewDate.Value = DateTime.Now.ToShortDateString();

                    if (ObjReviewList[0].IsFixed && ObjReviewList[0].IsOpen)
                    { objSelected.InnerText = ObjReviewList[0].FixedTitle; }
                    else
                    {
                        if (ObjReviewList[0].IsFixed && !ObjReviewList[0].IsOpen)
                        { objSelected.InnerText = ObjReviewList[0].FixedTitle; }
                        else
                        {
                            if (ObjReviewList[0].IsOpen && !ObjReviewList[0].IsFixed)
                            { objSelected.InnerText = ObjReviewList[0].OpenTitle; }
                        }
                    }
                    
                    reviewDate.Value = ObjReviewList[0].ReviewDate.ToString();
                    if (ObjReviewList[0].IsRating)
                    {
                        RatingsDropDownList.SelectedValue = ObjReviewList[0].Rating;
                        ratingRow.Attributes.CssStyle.Add("display", "block;");
                    }
                    
                    if (ObjReviewList[0].IsPerformance)
                    {
                        actual.Value = ObjReviewList[0].Actual;
                        target.Value = ObjReviewList[0].TargetValue.ToString();
                        performanceRow.Attributes.CssStyle.Add("display", "block;");
                    }
                    
                    if (ObjReviewList[0].IsComment)
                    {
                        comment.Value = ObjReviewList[0].Comment;
                        commentRow.Attributes.CssStyle.Add("display", "block;");
                    }
                    
                    ranking.Value = ObjReviewList[0].Rank.ToString();
                    objectiveID.Value = ObjReviewList[0].ObjectiveID.ToString();
                    reviewPeriodID.Value = ObjReviewList[0].ReviewPeriodID.ToString();
                    
                }
                catch (Exception ex)
                {
                    if (errors.InnerHtml.Length > 0)
                    {
                        String newLine = errors.InnerHtml + "<br/>" + "(Page_Load/Entry) :";
                        errors.InnerHtml = newLine + ex.Message;
                    }
                    else
                    {
                        errors.InnerHtml = "(Page_Load/Entry) : " + ex.Message;
                    }
                    errors.Visible = true;
                    messages.Visible = false;
                }
            }
        }

        private Boolean UpdateReview(Int32 objectiveID,
                                String userName,
                                DateTime reviewDate,
                                String rating,
                                String actual,
                                String comment,
                                String reviewPeriodID)
        {
            Boolean successfulUpdate = false;

            try
            {
                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("[sp_AddReview]", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@ObjectiveID", SqlDbType.Int).Value = objectiveID;
                        command.Parameters.Add("@Username", SqlDbType.VarChar).Value = userName;
                        command.Parameters.Add("@ReviewDate", SqlDbType.DateTime).Value = reviewDate;
                        command.Parameters.Add("@Rating", SqlDbType.VarChar).Value = rating;
                        command.Parameters.Add("@Comment", SqlDbType.VarChar).Value = comment;
                        command.Parameters.Add("@ReviewPeriodID", SqlDbType.VarChar).Value = reviewPeriodID;
                        if (actual != null)
                        {
                            command.Parameters.Add("@Actual", SqlDbType.Float).Value = Int32.Parse(actual);
                        }

                        SQLConnection.Open();

                        try
                        {
                            int updateCount = command.ExecuteNonQuery();

                            if (updateCount > 0)
                            {
                                messages.InnerText = "Your changes have been saved.";
                                messages.Visible = true;
                                errors.Visible = false;
                                successfulUpdate = true;
                            }
                            else
                            {
                                errors.InnerHtml = "Unable to save changes. Sorry, no further details are available.";
                                errors.Visible = true;
                                messages.Visible = false;
                            }
                        }
                        catch (Exception ex)
                        {
                            errors.InnerHtml = "An error occurred while attempting to save your changes: " + ex.Message;
                            errors.Visible = true;
                            messages.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(UpdateReview) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "An unexpected error has occurred (UpdateReview) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
            return successfulUpdate;
            
        }

        private void Authorise()
        {
            //String userName = Environment.UserName;
            String userName = System.Web.HttpContext.Current.User.Identity.Name;
            userName = userName.Substring(userName.LastIndexOf('\\') + 1);
            Boolean mgrFound = false;
            try
            {
                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_getManagerForPerson", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@StaffID", SqlDbType.VarChar).Value = StaffID; 

                        SQLConnection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                                if (!reader.HasRows)
                                {
                                    Server.Transfer("NotAuthorised.aspx", true);
                                }
                                else
                                {

                                    StaffName = reader["Forename"].ToString() + " " + reader["Surname"].ToString();
                                    MgrName = reader["mgrForename"].ToString() + " " + reader["mgrSurname"].ToString();
                                    MgrUserName = reader["mgrUsername"].ToString();

                                    if (userName == RequesterUserName && userName == MgrUserName)
                                    {
                                        mgrFound = true; ;
                                    }
                                }
                        }
                        //if (!mgrFound)
                        //{
                        //    Server.Transfer("NotAuthorised.aspx", true);
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(Authorise) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "An unexpected error has occurred (Authorise) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }

        private void GetReviewData()
        {
            try
            {
                ObjReviewList = new List<ObjectiveReview>();

                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_getReviewsForPerson", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@AcademicYear", SqlDbType.VarChar).Value = AcademicYear;
                        command.Parameters.Add("@ObjectiveTypeID", SqlDbType.VarChar).Value = ObjectiveTypeID;
                        command.Parameters.Add("@Term", SqlDbType.VarChar).Value = Term; //'MID-YEAR' or 'END OF YEAR'
                        command.Parameters.Add("@StaffID", SqlDbType.VarChar).Value = StaffID;

                        SQLConnection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (!reader.HasRows)
                            {
                                Server.Transfer("NoData.aspx", true);
                            }
                            while (reader.Read())
                            {
                                ObjReviewList.Add(new ObjectiveReview()
                                {
                                    ObjectiveID = reader["ObjectiveID"] is DBNull ? 0 : (int)reader["ObjectiveID"],
                                    ObjectiveTypeID = reader["ObjectiveTypeID"] is DBNull ? 0 : (int)reader["ObjectiveTypeID"],
                                    FixedTitle = reader["fo_title"] is DBNull ? "" : (string)reader["fo_title"],
                                    FixedText = reader["fo_text"] is DBNull ? "" : (string)reader["fo_text"],
                                    Rank = reader["ranking"] is DBNull ? 0 : (int)reader["ranking"],
                                    OpenTitle = reader["personal_title"] is DBNull ? "" : (string)reader["personal_title"],
                                    OpenText = reader["personal_text"] is DBNull ? "" : (string)reader["personal_text"],
                                    TypeName = reader["TypeName"] is DBNull ? "" : (string)reader["TypeName"],
                                    IsFixed = reader.GetBoolean(reader.GetOrdinal("IsFixed")),
                                    MidYearReq = reader.GetBoolean(reader.GetOrdinal("MidYearReq")),
                                    YearEndReq = reader.GetBoolean(reader.GetOrdinal("YearEndReq")),
                                    IsRating = reader.GetBoolean(reader.GetOrdinal("IsRating")),
                                    IsPerformance = reader.GetBoolean(reader.GetOrdinal("IsPerformance")),
                                    IsComment = reader.GetBoolean(reader.GetOrdinal("IsComment")),
                                    IsOpen = reader.GetBoolean(reader.GetOrdinal("IsOpen")),
                                    TargetValue = reader["TargetValue"] is DBNull ? "" : reader.GetDouble(reader.GetOrdinal("TargetValue")).ToString(),
                                    SupportRequirements = reader["SupportRequirements"] is DBNull ? "" : (string)reader["SupportRequirements"],
                                    CreatedDate = reader["CreatedDate"] is DBNull ? "" : (string)reader["CreatedDate"],
                                    CreatedBy = reader["CreatedBy"] is DBNull ? "" : (string)reader["CreatedBy"],
                                    ModifiedDate = reader["ModifiedDate"] is DBNull ? "" : (String)reader["ModifiedDate"],
                                    ModifiedBy = reader["ModifiedBy"] is DBNull ? "" : (string)reader["ModifiedBy"],
                                    ReviewID = reader["ReviewID"] is DBNull ? 0 : (int)reader["ReviewID"],
                                   // ReviewDate = reader["ReviewDate"] is DBNull ? "" : reader["ReviewDate"].ToString().Split('/')[1] + "/" + (string)reader["ReviewDate"].ToString().Split('/')[0] + "/" + (string)reader["ReviewDate"].ToString().Split('/')[2],
                                    ReviewDate = reader["ReviewDate"] is DBNull ? "" : reader["ReviewDate"].ToString(),
                                    Rating = reader["Rating"] is DBNull ? "" : (string)reader["Rating"],
                                    Actual = reader["Actual"] is DBNull ? "" : reader.GetDouble(reader.GetOrdinal("Actual")).ToString(),
                                    Comment = reader["Comment"] is DBNull ? "" : (string)reader["Comment"],
                                    ReviewPeriodID = reader["ReviewPeriodID"] is DBNull ? Term : (string)reader["ReviewPeriodID"]
                                });
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" +  "(GetReviewData) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "(GetReviewData) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }

        private void CreateRatingsDropdown()
        {
            try
            {
                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_getRatings", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        SQLConnection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                RatingsDropDownList.Items.Add(new ListItem((string)reader["RatingDescription"].ToString(),
                                                                            (string)reader["RatingID"].ToString()));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(CreateRatingsDropdown) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "An unexpected error has occurred (CreateRatingsDropdown) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }

        private void CreateObjectivesTable(Int32 fieldIndex = 1000) // 'fieldIndex' is a way of providing a consistent ID for dynamically created fields; without
                                                                    // this the controls are likely to behve erratically.
        {
            try
            {
                PlaceHolderObjectiveSummary.Controls.Clear();

                String title, line1, line2;

                // Create a Table and set its properties 
                Table tbl = new Table();
                tbl.Controls.Clear();
                tbl.ID = "ObjectivesTable";
                tbl.CssClass = "table-bordered";
                tbl.CellPadding = 10;
                //tbl.EnableViewState = true;


                // Add the table to the placeholder control
                PlaceHolderObjectiveSummary.Controls.Add(tbl);

                // Now iterate through the table and add controls 
                TableRow tr = new TableRow();
                Int32 ix = 0;

                foreach (ObjectiveReview review in ObjReviewList)
                {
                    TableCell tc = new TableCell();
                    tc.ID = review.Rank.ToString();
                    //tc.ID = fieldIndex++.ToString();
                    tc.Width = 5000;
                    tc.VerticalAlign = VerticalAlign.Top;
                    Label lblComment = new Label();

                    tc.Attributes.Add("onclick", "CellAction(this)");

                    HiddenField reviewDate = new HiddenField();
                    reviewDate.ID = fieldIndex++.ToString();
                    reviewDate.Value = review.ReviewDate.ToString();
                    HiddenField rating = new HiddenField();
                    rating.ID = fieldIndex++.ToString();
                    rating.Value = review.Rating;
                    HiddenField actual = new HiddenField();
                    actual.ID = fieldIndex++.ToString();
                    actual.Value = review.Actual;
                    HiddenField comment = new HiddenField();
                    comment.ID = fieldIndex++.ToString();
                    comment.Value = review.Comment;
                    HiddenField objectiveType = new HiddenField();
                    objectiveType.ID = fieldIndex++.ToString();
                    objectiveType.Value = review.ModifiedBy;
                    HiddenField ranking = new HiddenField();
                    ranking.ID = fieldIndex++.ToString();
                    ranking.Value = review.Rank.ToString();
                    HiddenField objectiveID = new HiddenField();
                    objectiveID.ID = fieldIndex++.ToString();
                    objectiveID.Value = review.ObjectiveID.ToString();
                    HiddenField reviewPeriodID = new HiddenField();
                    reviewPeriodID.ID = fieldIndex++.ToString();
                    reviewPeriodID.Value = review.ReviewPeriodID.ToString();
                    HiddenField target = new HiddenField();
                    target.ID = fieldIndex++.ToString();
                    target.Value = review.TargetValue.ToString();
                    HiddenField index = new HiddenField();
                    index.ID = fieldIndex++.ToString();
                    index.Value = ix.ToString();

                    if (rowIndex.Value.Length == 0 || Int32.Parse(rowIndex.Value) == ix)
                    {
                        tc.BackColor = System.Drawing.Color.Orange;
                        rowIndex.Value = rowIndex.Value = ix.ToString();
                    }
                    ix = ix + 1;

                    if (review.IsFixed && review.IsOpen)
                    {
                        title = review.FixedTitle;
                        objectiveType.Value = title;
                        line1 = review.FixedText;
                        line2 = review.OpenText;
                        lblComment.Text =
                            "<p><b>" + title + "</b></p>" +
                            "<p><i>" + line1 + "</i></p>" +
                            "<p>" + line2 + "</p>";
                    }
                    else
                    {
                        if (review.IsFixed && !review.IsOpen)
                        {
                            title = review.FixedTitle;
                            objectiveType.Value = title;
                            line1 = review.FixedText;
                            line2 = String.Empty;
                            lblComment.Text =
                                "<p><b>" + title + "</b></p>" +
                                "<p><i>" + line1 + "</i></p>";
                        }
                        else
                        {
                            if (review.IsOpen && !review.IsFixed)
                            {
                                title = review.OpenTitle;
                                objectiveType.Value = title;
                                line1 = review.OpenText;
                                line2 = String.Empty;
                                lblComment.Text =
                                    "<p><b>" + title + "</b></p>" +
                                    "<p>" + line1 + "</p>";
                            }
                        }
                    }

                    tc.Controls.Add(lblComment);
                    tc.Controls.Add(reviewDate);
                    tc.Controls.Add(rating);
                    tc.Controls.Add(actual);
                    tc.Controls.Add(comment);
                    tc.Controls.Add(objectiveType);
                    tc.Controls.Add(ranking);
                    tc.Controls.Add(objectiveID);
                    tc.Controls.Add(reviewPeriodID);
                    tc.Controls.Add(target);
                    tc.Controls.Add(index);

                    // Add the TableCell to the TableRow
                    tr.Cells.Add(tc);
                }

                tbl.Rows.Add(tr);

                // This parameter helps determine in the LoadViewState event,
                // whether to recreate the dynamic controls or not
                ViewState["dynamictable"] = true;
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(CreateObjectivesTable) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "An unexpected error has occurred (CreateObjectivesTable) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }

        private void GetMidYearSummary()
        {
            try
            {
                List<MidTermSummary> objectiveSummary = new List<MidTermSummary>();

                using (SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["HRAppraisals"].ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("sp_getMidTermSummaryForPerson", SQLConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@AcademicYear", SqlDbType.VarChar).Value = AcademicYear;
                        command.Parameters.Add("@ObjectiveTypeID", SqlDbType.VarChar).Value = ObjectiveTypeID;
                        command.Parameters.Add("@StaffID", SqlDbType.VarChar).Value = StaffID;

                        SQLConnection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                objectiveSummary.Add(new MidTermSummary()
                                {
                                    Title = reader["Title"].ToString(),
                                    ReviewDate = reader["ReviewDate"].ToString(),
                                    Rating = reader["Rating"].ToString(),
                                    Actual = reader["Actual"].ToString(),
                                    Comment = reader["Comment"].ToString()
                                });
                            }
                        }
                    }
                }

                midTermSummary.DataSource = objectiveSummary;
                midTermSummary.DataBind();
            }
            catch (Exception ex)
            {
                if (errors.InnerHtml.Length > 0)
                {
                    String newLine = errors.InnerHtml + "<br/>" + "(GetMidYearSummary) :";
                    errors.InnerHtml = newLine + ex.Message;
                }
                else
                {
                    errors.InnerHtml = "An unexpected error has occurred (GetMidYearSummary) : " + ex.Message;
                }
                errors.Visible = true;
                messages.Visible = false;
            }
        }
    }
}