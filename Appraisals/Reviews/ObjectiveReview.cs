﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Appraisals.Reviews
{
   

    public class FixedObjective
    {
        public int FixedObjectiveID { get; set; }
        public int ObjectiveTypeID { get; set; }
        public String ObjectiveTitle { get; set; }
        public String TypeName { get; set; }
        public Boolean IsSupportReq { get; set; }
    }

    public class KPI
    {
        public int ObjectiveID { get; set; }
        public DateTime SetDate { get; set; }
        public String Title { get; set; }
        public int Rank { get; set; }
        public String TargetValue { get; set; }
        public String ReviewDate { get; set; }
        public String Rating { get; set; }
        public String Actual { get; set; }
        public String Comment { get; set; }
    }

    public class Comments
    {
        public int CommentID { get; set; }
        public String ReviewPeriodID { get; set; }
        public String TLARating { get; set; }
        public String OverallRating { get; set; }
        public String EmployeeCommentAppraisal { get; set; }
        public String EmployeeCommentPerformance { get; set; }
        public String ManagerComment { get; set; }
        public Boolean EmployeeOpportunity { get; set; }
        public Boolean ReviewComplete { get; set; }
        public String CreatedBy { get; set; }
        public String CreatedDate { get; set; }
        public String ModifiedBy { get; set; }
        public String ModifiedDate { get; set; }
        public int StaffType { get; set; }
        public String CPDComment { get; set; }
        public Boolean CPDInPlace { get; set; }
        public Boolean SkillsQuestionnaireComplete { get; set; }
        public int CPDHours { get; set; }
        public String CPDRating { get; set; }


    }

    public class ObjectiveReview
    {
        public int ObjectiveID { get; set; }
        public int ObjectiveTypeID { get; set; }
        public String SetDate { get; set; }
        public String FixedTitle { get; set; }
        public String FixedText { get; set; }
        public String OpenTitle { get; set; }
        public String OpenText { get; set; }
        public int Rank { get; set; }
        public String TypeName { get; set; }
        public Boolean IsFixed { get; set; }
        public Boolean MidYearReq { get; set; }
        public Boolean YearEndReq { get; set; }
        public Boolean IsRating { get; set; }
        public Boolean IsPerformance { get; set; }
        public Boolean IsComment { get; set; }
        public Boolean IsOpen { get; set; }
        public Boolean IsActive { get; set; }
        public String TargetValue { get; set; }
        public String SupportRequirements { get; set; }
        public String CreatedDate { get; set; }
        public String CreatedBy { get; set; }
        public String ModifiedDate { get; set; }
        public String ModifiedBy { get; set; }
        public int ReviewID { get; set; }
        public String ReviewDate { get; set; }
        public String Rating { get; set; }
        public String Actual { get; set; }
        public String Comment { get; set; }
        public String ReviewPeriodID { get; set; }
    }

    public class MidTermSummary
    {
        public String Title { get; set; }
        public String ReviewDate { get; set; }
        public String Rating { get; set; }
        public String Actual { get; set; }
        public String Comment { get; set; }
    }

    public class ObjectivesSummary
    {
        public String ObjectiveType { get; set; }
        public String CreatedDate { get; set; }
        public String ObjTitle { get; set; }
        public String ObjText { get; set; }
    }
}